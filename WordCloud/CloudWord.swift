//
//  CloudWord.swift
//  WordCloud
//
//  Created by Igor Medelian on 2/25/19.
//  Copyright © 2019 imedelian. All rights reserved.
//

import UIKit

public struct CloudWord {
    let title: String
    var weight: Float
    let color: UIColor
    
    init(title: String, weight: Float, color: UIColor = .clear) {
        self.title = title
        self.weight = weight
        self.color = color
    }
}
