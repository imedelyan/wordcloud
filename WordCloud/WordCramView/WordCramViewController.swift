//
//  WordCloudViewController.swift
//  WordCloud
//
//  Created by Igor Medelian on 2/22/19.
//  Copyright © 2019 imedelian. All rights reserved.
//

import UIKit

class WordCramViewController: UIViewController {

    @IBOutlet weak var wordCloudImageView: UIImageView!
    @IBOutlet weak var amountSlider: UISlider!
    @IBOutlet weak var fontSizeSlider: UISlider!
    
    let wordCloudGenerator = WordCloudGenerator()
    
    let red = UIColor(red: 232/255, green: 101/255, blue: 148/255, alpha: 1.0)
    let blue = UIColor(red: 69/255, green: 166/255, blue: 242/255, alpha: 1.0)
    let green = UIColor(red: 103/255, green: 187/255, blue: 106/255, alpha: 1.0)
    let yellow = UIColor(red: 231/255, green: 175/255, blue: 43/255, alpha: 1.0)
    
    lazy var words: [CloudWord] = [CloudWord(title: "Brave", weight: 1, color: yellow),
                                   CloudWord(title: "Stressed", weight: 1, color: blue),
                                   CloudWord(title: "Inadequate", weight: 1, color: blue),
                                   CloudWord(title: "Self-critical", weight: 1, color: blue),
                                   CloudWord(title: "Patient", weight: 1, color: green),
                                   CloudWord(title: "Peaceful", weight: 1, color: yellow),
                                   CloudWord(title: "Valuable", weight: 1, color: yellow),
                                   CloudWord(title: "Calm", weight: 1, color: green),
                                   CloudWord(title: "Distant", weight: 1, color: blue),
                                   CloudWord(title: "Downhearted", weight: 1, color: red),
                                   CloudWord(title: "Anxious", weight: 1, color: red),
                                   CloudWord(title: "A Failure", weight: 1, color: red),
                                   CloudWord(title: "Ashamed", weight: 1, color: red),
                                   CloudWord(title: "Happy", weight: 1, color: yellow),
                                   CloudWord(title: "Hopeful", weight: 1, color: green),
                                   CloudWord(title: "Content", weight: 1, color: green),
                                   CloudWord(title: "Accepting", weight: 1, color: green),
                                   CloudWord(title: "Kind", weight: 1, color: yellow)]
    var currentWords: [CloudWord] = []
    var headWordIndex: Int = 0
    
//    let wordsImageSize = CGSize(width: 500, height: 500)
    var wordAmountSliderIndex = 0
    
    @IBAction func didMoveWordAmountSlider(_ sender: UISlider) {
        guard wordAmountSliderIndex != Int(floor(sender.value)) else { return }
        wordAmountSliderIndex = Int(floor(sender.value))
        
        currentWords = []
        for i in 0..<wordAmountSliderIndex {
            currentWords.insert(words[i], at: currentWords.startIndex)
        }
//        WordCramGenerator.generateFromWords(words: currentWords, size: wordsImageSize) { [weak self] image in
//            self?.wordCloudImageView.image = image
//        }
        
        fontSizeSlider.setValue(Float(currentWords.first?.weight ?? 0), animated: true)
        
        wordCloudGenerator.generate(from: currentWords) { [weak self] image in
            self?.wordCloudImageView.image = image
        }
    }
    
    @IBAction func didMoveFontSizeSlider(_ sender: UISlider) {
        words[wordAmountSliderIndex - 1].weight = sender.value
        currentWords[0].weight = sender.value
        wordCloudGenerator.generate(from: currentWords) { [weak self] image in
            self?.wordCloudImageView.image = image
        }
    }
}
