//
//  WordCloudGenerator.swift
//  TruPic
//

import UIKit
import WordCram

class WordCramGenerator {
    
    class func generateFromWords(words: [CloudWord], size: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        let context = UIGraphicsGetCurrentContext()
        
        let wcWords = words.map { WCWord(word: $0.title, weight: Double($0.weight)) }
        for i in 0..<words.count {
            wcWords[i].setColor(color: words[i].color)
        }
        
        let wordCram = WordCram(ctx: context!)
            .fromWords(words: wcWords)
            .withPlacer(placer: Placers.centerClump())
            .withAngler(angler: Anglers.random())
            .withFont(font: UIFont.systemFont(ofSize: 2))
            .withColorer(colorer: Colorers.alwaysUse(color: UIColor.white))
            .withSizer(sizer: Sizers.byWeight(minSize: 5, maxSize: 95))
            .maxNumberOfWordsToDraw(maxWords: 100)
            .withWordPadding(padding: 10)
        
        wordCram.drawAll()
        
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return image
    }
    
    class func generateFromWords(words: [CloudWord], size: CGSize, completion: @escaping (UIImage) -> Void) {
        DispatchQueue.global(qos: .background).async {
            let image = self.generateFromWords(words: words, size: size)
            DispatchQueue.main.async {
                completion(image)
            }
        }
    }
}
