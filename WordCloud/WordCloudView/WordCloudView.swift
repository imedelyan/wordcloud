//
//  WordCloudView.swift
//  WordCloud
//
//  Created by Igor Medelian on 2/14/19.
//  Copyright © 2019 imedelian. All rights reserved.
//

import UIKit

//  Usage:
//
//    class func generateFromWords(words: [CloudWord], size: CGSize, completion: @escaping (UIImage) -> Void) {
//        let wordCloudView = WordCloudView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: size))
//        wordCloudView.words = words
//        wordCloudView.generate() {
//            let renderer = UIGraphicsImageRenderer(size: wordCloudView.bounds.size)
//            let image = renderer.image { ctx in
//                wordCloudView.drawHierarchy(in: wordCloudView.bounds, afterScreenUpdates: true)
//            }
//            completion(image)
//        }
//    }

open class WordCloudView: UIView {
    
    public var words: [CloudWord] = []
    @IBInspectable public var fontName: String = ""
    @IBInspectable public var minFontSize: Float = 14
    @IBInspectable public var maxFontSize: Float = 60
    @IBInspectable public var rotatable: Bool = true
    @IBInspectable public var themeColor: UIColor = .white
    public var colors: [UIColor] = [.blue, .red, .yellow, .green]
    public var onLabelTap: ((_ label: UILabel, _ title: String, _ index: Int) -> Void)?
    public var lastWordPosition: CGPoint = CGPoint.zero
    
    private var labels: [UILabel] = []
    public typealias GenerateCloudCompletion = () -> Void
    typealias AddLabelCompletion = (_ success: Bool) -> Void
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func setup() {
        
    }
    
    public func generate(completion: @escaping GenerateCloudCompletion) {
        guard words.count > 0 else { completion(); return }
        
        if labels.count > 0 {
            labels.forEach { $0.removeFromSuperview() }
            labels.removeAll()
        }
        
        words = words.sorted(by: { $0.weight > $1.weight })
        let maxWeight = words.first!.weight
        let minWeight = words.last!.weight
        
        let diff = maxWeight - minWeight
        
        var addedLabelsCount = 0
        for i in 0...words.count - 1 {
            let label = UILabel()
            label.tag = i
            label.text = words[i].title
            label.textColor = words[i].color
//            label.textColor = colors.randomElement()
//            label.textColor = i == 0 ? themeColor : themeColor.darker(by: CGFloat((10...70).randomElement()!))
            
            let fontSize = CGFloat(Float(words[i].weight) / 10 * (maxFontSize - minFontSize) + minFontSize)
            label.font = UIFont(name: fontName, size: fontSize) ?? UIFont.boldSystemFont(ofSize: fontSize)
//            let delta = words[i].weight - minWeight
//            var ratio = Float(delta) / Float(diff)
//            if diff == 0 { ratio = 1.0 }
//            label.font = sizeRatioFont(ratio)
            
            add(label: label) { success in
                success ? addedLabelsCount += 1 : nil
            }
        }
        
        completion()
    }
    
    func add(label: UILabel, completionHandler: @escaping AddLabelCompletion) {
        label.sizeToFit()
        let centerX = bounds.width / 2
        let centerY = bounds.height / 2
        var x = (bounds.width - label.bounds.width) / 2
        var y = (bounds.height - label.bounds.height) / 2
        var radius = CGFloat(hypotf(Float(centerX - x), Float(centerY - y)))
        var angle: CGFloat = atan2(y - centerY, x - centerX)
        let step: CGFloat = 1
        var cntTry = 0

        repeat {
            x = centerX + radius * cos(angle)
            y = centerY + radius * sin(angle)
            label.frame = CGRect(x: x, y: y, width: CGFloat(label.bounds.width), height: CGFloat(label.bounds.height))
            guard cntTry < 10000 else {
                completionHandler(false)
                return
            }
            cntTry += 1
            radius += 0.05
            angle += step / radius
        } while frameIntersects(label.frame)
        
        labels.append(label)
        addSubview(label)
        
        let tapGestue = UITapGestureRecognizer(target: self, action: #selector(self.handleGesture))
        label.addGestureRecognizer(tapGestue)
        label.isUserInteractionEnabled = true

        completionHandler(true)
    }
    
    @objc func handleGesture(_ gestureRecognizer: UIGestureRecognizer) {
        guard let label = (gestureRecognizer.view as? UILabel) else { return }
        onLabelTap?(label, label.text!, label.tag)
    }
    
    func sizeRatioFont(_ ratio: Float) -> UIFont {
        let fontSize = CGFloat(ratio * (maxFontSize - minFontSize) + minFontSize)
        return UIFont(name: fontName, size: fontSize) ?? UIFont.boldSystemFont(ofSize: fontSize)
    }
    
    func frameIntersects(_ frame: CGRect) -> Bool {
        guard frame.maxX < self.frame.width, frame.maxY < self.frame.height, frame.minX > 0, frame.minY > 0 else { return true }
        for label in labels where frame.intersects(label.frame) { return true }
        return false
    }
}

extension UIColor {
    
    func lighter(by percentage: CGFloat = 30.0) -> UIColor? {
        return self.adjust(by: abs(percentage) )
    }
    
    func darker(by percentage: CGFloat = 30.0) -> UIColor? {
        return self.adjust(by: -1 * abs(percentage) )
    }
    
    func adjust(by percentage: CGFloat = 30.0) -> UIColor? {
        var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 0
        if self.getRed(&red, green: &green, blue: &blue, alpha: &alpha) {
            return UIColor(red: min(red + percentage/100, 1.0),
                           green: min(green + percentage/100, 1.0),
                           blue: min(blue + percentage/100, 1.0),
                           alpha: alpha)
        } else {
            return nil
        }
    }
}

extension UIView {
    func rotate(degrees: CGFloat) {
        rotate(radians: CGFloat.pi * degrees / 180.0)
    }
    
    func rotate(radians: CGFloat) {
        self.transform = CGAffineTransform(rotationAngle: radians)
    }
}
