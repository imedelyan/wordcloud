//
//  ViewController.swift
//  WordCloud
//
//  Created by Igor Medelian on 2/14/19.
//  Copyright © 2019 imedelian. All rights reserved.
//

import UIKit

class WordCloudViewController: UIViewController {

    @IBOutlet weak var wordCloudView: WordCloudView!
    
    let red = UIColor(red: 232/255, green: 125/255, blue: 166/255, alpha: 1.0)
    let blue = UIColor(red: 127/255, green: 196/255, blue: 131/255, alpha: 1.0)
    let green = UIColor(red: 123/255, green: 167/255, blue: 216/255, alpha: 1.0)
    let yellow = UIColor(red: 249/255, green: 218/255, blue: 64/255, alpha: 1.0)
    
    lazy var words: [CloudWord] = [CloudWord(title: "Brave", weight: 200, color: yellow),
                                   CloudWord(title: "Calm", weight: 150, color: blue),
                                   CloudWord(title: "Word", weight: 120, color: blue),
                                   CloudWord(title: "Word", weight: 120, color: blue),
                                   CloudWord(title: "Beaten", weight: 100, color: red),
                                   CloudWord(title: "Happy", weight: 70, color: yellow),
                                   CloudWord(title: "Happy", weight: 70, color: yellow),
                                   CloudWord(title: "Word", weight: 60, color: green),
                                   CloudWord(title: "Word", weight: 30, color: blue),
                                   CloudWord(title: "Anxious", weight: 30, color: red),
                                   CloudWord(title: "Anxious", weight: 30, color: red),
                                   CloudWord(title: "Anxious", weight: 30, color: red),
                                   CloudWord(title: "Beaten", weight: 30, color: red),
                                   CloudWord(title: "Beaten", weight: 30, color: red),
                                   CloudWord(title: "Stressed", weight: 10, color: green),
                                   CloudWord(title: "Unstoppable", weight: 10, color: green),
                                   CloudWord(title: "Unstoppable", weight: 10, color: green),
                                   CloudWord(title: "Unstoppable", weight: 10, color: green)]

    let wordsImageSize = CGSize(width: 500, height: 500)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        wordCloudView.words = words
        wordCloudView.generate() {}
    }
}
