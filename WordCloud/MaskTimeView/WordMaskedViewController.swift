//
//  WordMaskedViewController.swift
//  WordCloud
//
//  Created by Igor Medelian on 3/1/19.
//  Copyright © 2019 imedelian. All rights reserved.
//

import UIKit

class WordMaskedViewController: UIViewController {

    @IBOutlet weak var wordCloudImageView: UIImageView!
    
    let wordCloudGenerator = WordMaskedGenerator()
    
    let red = UIColor(red: 232/255, green: 101/255, blue: 148/255, alpha: 1.0)
    let blue = UIColor(red: 69/255, green: 166/255, blue: 242/255, alpha: 1.0)
    let green = UIColor(red: 103/255, green: 187/255, blue: 106/255, alpha: 1.0)
    let yellow = UIColor(red: 231/255, green: 175/255, blue: 43/255, alpha: 1.0)
    
    let cloudWordsCount = 18
    
    lazy var words: [CloudWord] = [CloudWord(title: "OK", weight: 25, color: red),
                                   CloudWord(title: "yes", weight: 1, color: blue),
                                   CloudWord(title: "yes", weight: 1, color: green),
                                   CloudWord(title: "yes", weight: 1, color: yellow),

                                   CloudWord(title: "Anxious", weight: 1, color: red),
                                   CloudWord(title: "Stressed", weight: 1, color: blue),
                                   CloudWord(title: "Calm", weight: 1, color: green),
                                   CloudWord(title: "Peaceful", weight: 1, color: yellow),

                                   CloudWord(title: "A Failure", weight: 1, color: red),
                                   CloudWord(title: "Inadequate", weight: 1, color: blue),
                                   CloudWord(title: "Content", weight: 1, color: green),
                                   CloudWord(title: "Valuable", weight: 1, color: yellow),

                                   CloudWord(title: "Ashamed", weight: 1, color: red),
                                   CloudWord(title: "Self-critical", weight: 1, color: blue),
                                   CloudWord(title: "Accepting", weight: 1, color: green),
                                   CloudWord(title: "Happy", weight: 1, color: yellow),

                                   CloudWord(title: "no", weight: 1, color: red),
                                   CloudWord(title: "no", weight: 1, color: blue),
                                   CloudWord(title: "no", weight: 1, color: green),
                                   CloudWord(title: "no", weight: 1, color: yellow),

                                   CloudWord(title: "why", weight: 1, color: red),
                                   CloudWord(title: "why", weight: 1, color: blue),
                                   CloudWord(title: "why", weight: 1, color: green),
                                   CloudWord(title: "why", weight: 1, color: yellow)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        wordCloudGenerator.generate(from: words) { [weak self] image in
            self?.wordCloudImageView.image = image
        }
    }
}
