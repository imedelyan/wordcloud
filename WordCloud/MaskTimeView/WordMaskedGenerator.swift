//
//  WordMaskedGenerator.swift
//  WordCloud
//
//  Created by Igor Medelian on 3/1/19.
//  Copyright © 2019 imedelian. All rights reserved.
//

import UIKit

open class WordMaskedGenerator {
    
    private let fontName: String
    private var minFontSize: Float = 20
    private var maxFontSize: Float = 70
    private var words: [CloudWord] = []
    private var labels: [UILabel] = []
    private let size = CGSize(width: 300, height: 300)
    private typealias GenerateCloudCompletion = () -> Void
    private lazy var currentImage: RGBAImage? = nil
    
    // MARK: - Init
    public init(fontName: String = "") {
        self.fontName = fontName
    }
    
    // MARK: - Public
    public func generate(from words: [CloudWord], completion: @escaping (UIImage) -> Void) {
        currentImage = RGBAFactory.makeEmptyImage(of: size)
        self.words = words
        generate() { [weak self] in
            completion(self!.currentImage!.toUIImage()!) // TODO: resolve force unwrap
        }
    }
    
    // MARK: - Private
    private func generate(completion: @escaping GenerateCloudCompletion) {
        guard words.count > 0 else { completion(); return }
        
        if labels.count > 0 {
            labels.forEach { $0.removeFromSuperview() }
            labels.removeAll()
        }
        
        for i in 0...words.count - 1 {
            let label = wordLabel(for: i)
            add(label: label)
        }
        
        completion()
    }
    
    private func add(label: UILabel) {
        let centerX = size.width / 2
        let centerY = size.height / 2
        var labelCenterX = centerX
        var labelCenterY = centerY
        var radius: CGFloat = 0
        var angle: CGFloat = 0
        let step: CGFloat = 15
        var cntTry = 0
        
        var addingLableToImageResult = false
        repeat {
            labelCenterX = centerX + radius * cos(angle)
            labelCenterY = centerY + radius * sin(angle)
            let x = labelCenterX - label.bounds.width / 2
            let y = labelCenterY - label.bounds.height / 2
            label.frame = CGRect(x: x, y: y, width: CGFloat(label.bounds.width), height: CGFloat(label.bounds.height))
            guard cntTry < 12000 else { return }
            cntTry += 1
            radius += 1
            angle += step / radius
            
            let newImage = label.image()

            guard !frameOutOfBounds(label.frame) else {
                return
            }
            
            addingLableToImageResult = add(image: RGBAImage(image: newImage)!, at: CGPoint(x: x, y: y))
        } while !addingLableToImageResult
        
        labels.append(label)
    }
    
    private func add(image: RGBAImage, at point: CGPoint) -> Bool {
        let x = Int(point.x)
        let y = Int(point.y)
        
        let resultImage = currentImage!
        
        // compare parts of two images
        for a in y..<(y + image.height) {
            for b in x..<(x + image.width) {
                let index = a * currentImage!.width + b
                let imageIndex = (a - y) * image.width + (b - x)
                
                let currentPixel = currentImage!.pixels[index]
                let newPixel = image.pixels[imageIndex]
                
                guard currentPixel.isEmpty || newPixel.isEmpty else {
                    return false
                }
            }
        }
        
        // merge parts of images
        for a in y..<(y + image.height) {
            for b in x..<(x + image.width) {
                let index = a * currentImage!.width + b
                let imageIndex = (a - y) * image.width + (b - x)
                
                let currentPixel = currentImage!.pixels[index]
                let newPixel = image.pixels[imageIndex]
                
                resultImage.pixels[index] = currentPixel.isEmpty ? newPixel : currentPixel
            }
        }

        currentImage = resultImage
        return true
    }
    
    // MARK: - Helpers
    private func wordLabel(for index: Int) -> UILabel {
        let label = UILabel()
        label.text = words[index].title
        label.textColor = words[index].color
        label.font = sizeRatioFont(words[index].weight)
        label.sizeToFit()
        label.frame = CGRect(x: 0, y: 0, width: label.frame.width + 10, height: label.frame.height)
        return label
    }
    
    private func sizeRatioFont(_ ratio: Float) -> UIFont {
        let fontSize = CGFloat(ratio / 10 * (maxFontSize - minFontSize) + minFontSize)
        return UIFont(name: fontName, size: fontSize) ?? UIFont.boldSystemFont(ofSize: fontSize)
    }
    
    private func frameOutOfBounds(_ frame: CGRect) -> Bool {
        return frame.maxX > size.width || frame.maxY > size.height || frame.minX < 0 || frame.minY < 0
    }
}

extension UIView {
    func image() -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: bounds.size)
        let image = renderer.image { ctx in
            self.drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        }
        return image
    }
}
