//
//  RGBAImage.swift
//  WordCloud
//
//  Created by Igor Medelian on 3/7/19.
//  Copyright © 2019 imedelian. All rights reserved.
//

import UIKit

struct RGBAImage {
    var pixels: UnsafeMutableBufferPointer<Pixel>
    var width: Int
    var height: Int
    
    init(pixels: UnsafeMutableBufferPointer<Pixel>, width: Int, height: Int) {
        self.pixels = pixels
        self.width = width
        self.height = height
    }
    
    init?(image: UIImage?) {
        guard let image = image, let cgImage = image.cgImage else { return nil }
        
        width = Int(image.size.width)
        height = Int(image.size.height)
        
        let bitsPerComponent = 8
        let bytesPerPixel = 4
        let bytesPerRow = width * bytesPerPixel
        let imageData = UnsafeMutablePointer<Pixel>.allocate(capacity: width * height)
        
        // we need to reset all pixels in buffer to remove sideeffects
        for y in 0..<height {
            for x in 0..<width {
                let index = y * width + x
                imageData[index] = Pixel(red: 0, green: 0, blue: 0, alpha: 0)
            }
        }
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        
        var bitmapInfo: UInt32 = CGBitmapInfo.byteOrder32Big.rawValue
        bitmapInfo |= CGImageAlphaInfo.premultipliedLast.rawValue & CGBitmapInfo.alphaInfoMask.rawValue
        guard let imageContext = CGContext(data: imageData, width: width, height: height, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo) else { return nil }
        imageContext.draw(cgImage, in: CGRect(origin: CGPoint.zero, size: image.size))
        
        pixels = UnsafeMutableBufferPointer<Pixel>(start: imageData, count: width * height)
    }
    
    func toUIImage() -> UIImage? {
        let bitsPerComponent = 8
        let bytesPerPixel = 4
        let bytesPerRow = width * bytesPerPixel
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        
        var bitmapInfo: UInt32 = CGBitmapInfo.byteOrder32Big.rawValue
        bitmapInfo |= CGImageAlphaInfo.premultipliedLast.rawValue & CGBitmapInfo.alphaInfoMask.rawValue
        guard let imageContext = CGContext(data: pixels.baseAddress, width: width, height: height, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo, releaseCallback: nil, releaseInfo: nil) else { return nil }
        guard let cgImage = imageContext.makeImage() else { return nil }
        
        let image = UIImage(cgImage: cgImage)
        return image
    }
}

class RGBAFactory {
    class func makeEmptyImage(of size: CGSize) -> RGBAImage {
        let pixelCount = Int(size.width * size.height)
        let imageData = UnsafeMutablePointer<Pixel>.allocate(capacity: pixelCount)
        let pixels = UnsafeMutableBufferPointer(start: imageData, count: pixelCount)
        
        for i in 0..<pixelCount {
            pixels[i] = Pixel(red: 0, green: 0, blue: 0, alpha: 0)
        }
        
        let emptyImage = RGBAImage(pixels: pixels, width: Int(size.width), height: Int(size.height))
        return emptyImage
    }
}
