//  Sizers.swift

import UIKit

public class Sizers {
    
    class ConcreteWordSizer: WordSizer {
        var minSize: Double
        var maxSize: Double
        
        init(_ minSize: Double, _ maxSize: Double) {
            self.minSize = minSize
            self.maxSize = maxSize
        }
        
        func sizeFor(word: WCWord, wordRank: Int, wordCount: Int) -> Double {
            return 0
        }
    }
    
    private class ByWeightWordSizer: ConcreteWordSizer {
        override func sizeFor(word: WCWord, wordRank: Int, wordCount: Int) -> Double {
            return PApplet.lerp(start: minSize, stop: maxSize, amt: word.weight)
        }
    }
    
    private class ByRankWordSizer: ConcreteWordSizer {
        override func sizeFor(word: WCWord, wordRank: Int, wordCount: Int) -> Double {
            return PApplet.map(value: Double(wordRank), start1: 0, stop1: Double(wordCount), start2: maxSize, stop2: minSize)
        }
    }
    
    
    public class func byWeight(minSize: Int, maxSize: Int) -> WordSizer {
        return ByWeightWordSizer(Double(minSize), Double(maxSize))
    }
    
    public class func byRank(minSize: Int, maxSize: Int) -> WordSizer {
        return ByRankWordSizer(Double(minSize), Double(maxSize))
    }
    
}
