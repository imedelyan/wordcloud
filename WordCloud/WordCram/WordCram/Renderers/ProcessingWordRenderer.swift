//  ProcessingWordRenderer.swift

import UIKit

class ProcessingWordRenderer: WordRenderer {
    
    private var context: CGContext
    
    init(context: CGContext) {
        self.context = context
    }
    
    func getWidth() -> Int {
        return Int(Double(context.width) / Double(UIScreen.main.scale))
    }
    
    func getHeight() -> Int {
        return Int(Double(context.width) / Double(UIScreen.main.scale))
    }
    
    func drawWord(word: EngineWord, color: UIColor) {
        let font = word.word.renderedFont
        let fontSize = CGFloat(word.word.renderedSize)
        let color = word.word.renderedColor
        let attrs = [
            NSAttributedString.Key.font: font?.withSize(fontSize),
            NSAttributedString.Key.foregroundColor: color
        ]
        let string = word.word.word as NSString
        guard let position = word.word.renderedPlace else { return }
        let size = string.size(withAttributes: attrs as [NSAttributedString.Key : Any])
        _ = CGRect(origin: position, size: size)

        print("drawing word at \(String(describing: position))")
        print(word.word.word)
//        string.drawInRect(CGRectIntegral(rect), withAttributes: attrs)
        
        guard let cfString = CFAttributedStringCreate(nil, string, attrs as CFDictionary) else { return }
        let line = CTLineCreateWithAttributedString(cfString)
        _ = CTLineGetImageBounds(line, context)

        context.textPosition = CGPoint(x: position.x, y: position.y)
        CTLineDraw(line, context)
    }
    
    func finish() {}
}
