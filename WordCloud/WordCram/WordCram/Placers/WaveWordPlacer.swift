//  WaveWordPlacer.swift

import UIKit

public class WaveWordPlacer: WordPlacer {
    
    public func place(word: WCWord, wordIndex: Int, wordsCount: Int, wordImageWidth: Int, wordImageHeight: Int, fieldWidth: Int, fieldHeight: Int) -> CGPoint {
        let normalizedIndex = Double(wordIndex) / Double(wordsCount)
        let x = normalizedIndex * Double(fieldWidth)
        let y = normalizedIndex * Double(fieldHeight)
        
        let yOffset = getYOffset(wordIndex: wordIndex, wordCount: wordsCount, fieldHeight: fieldHeight)
        return CGPoint(x: x, y: y + yOffset)
    }
    
    private func getYOffset(wordIndex: Int, wordCount: Int, fieldHeight: Int) -> Double {
        let theta = PApplet.map(value: Double(wordIndex), start1: 0, stop1: Double(wordCount), start2: Double.pi, stop2: -Double.pi)
        return sin(theta) / (Double(fieldHeight) / 3)
    }
    
}
