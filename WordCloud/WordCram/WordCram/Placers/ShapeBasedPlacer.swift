//  ShapeBasedPlacer.swift

import UIKit

public class ShapeBasedPlacer: WordPlacer, WordNudger {
    
    private var area: CGRect
    
    private var minX: CGFloat
    private var minY: CGFloat
    private var maxX: CGFloat
    private var maxY: CGFloat

    public init(shape: CGRect) {
        self.area = shape
        minX = area.minX
        minY = area.minY
        maxX = area.maxX
        maxY = area.maxY
    }

    public func place(word: WCWord, wordIndex: Int, wordsCount: Int, wordImageWidth: Int, wordImageHeight: Int, fieldWidth: Int, fieldHeight: Int) -> CGPoint {
        for _ in 0..<1000 {
            let newX = CGFloat(Double.random(Double(minX), Double(maxX)))
            let newY = CGFloat(Double.random(Double(minY), Double(maxY)))
            if area.contains(CGRect(x: newX, y: newY, width: CGFloat(wordImageWidth), height: CGFloat(wordImageHeight))) {
                return CGPoint(x: newX, y: newY)
            }
        }
        
        return CGPoint(x: -1, y: -1)
    }
    
    public func nudgeFor(word: WCWord, attemptNumber: Int) -> CGPoint {
        let target = word.targetPlace!
        let wx = target.x
        let wy = target.y
        let ww = word.renderedWidth
        let wh = word.renderedHeight
        
        for _ in 0..<1000 {
            let newX = CGFloat(Double.random(Double(minX), Double(maxX)))
            let newY = CGFloat(Double.random(Double(minY), Double(maxY)))
            if area.contains(CGRect(x: newX, y: newY, width: wh, height: ww)) {
                return CGPoint(x: newX - wx, y: newY - wy)
            }
        }
        
        return CGPoint(x: -1, y: -1)
    }
    
}
