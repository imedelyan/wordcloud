//  PlottingWordPlacer.swift

import UIKit

public class PlottingWordPlacer: WordPlacer {
    
    private var context: CGContext
    private var wrapperPlacer: WordPlacer
    
    public init(context: CGContext, wrapperPlacer: WordPlacer) {
        self.context = context
        self.wrapperPlacer = wrapperPlacer
    }
    
    public func place(word: WCWord, wordIndex: Int, wordsCount: Int, wordImageWidth: Int, wordImageHeight: Int, fieldWidth: Int, fieldHeight: Int) -> CGPoint {
        let v = wrapperPlacer.place(word: word, wordIndex: wordIndex, wordsCount: wordsCount, wordImageWidth: wordImageWidth, wordImageHeight: wordImageHeight, fieldWidth: fieldWidth, fieldHeight: fieldHeight)
        
        context.saveGState()
        context.setFillColor(UIColor.clear.cgColor)
        
        let strokeColor = UIColor(red: 15/255, green: 255/255, blue: 255/255, alpha: 200/255).cgColor
        context.setStrokeColor(strokeColor)
        let rect = CGRect(x: v.x, y: v.y, width: 10, height: 10)
        context.fillEllipse(in: rect)

        context.restoreGState()
        return v
    }
    
}
