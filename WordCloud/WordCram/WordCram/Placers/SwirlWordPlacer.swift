//  SwirlWordPlacer.swift

import UIKit

public class SwirlWordPlacer: WordPlacer {
    
    public func place(word: WCWord, wordIndex: Int, wordsCount: Int, wordImageWidth: Int, wordImageHeight: Int, fieldWidth: Int, fieldHeight: Int) -> CGPoint {
        let normalizedIndex = Double(wordIndex) / Double(wordsCount)
        
        let theta = normalizedIndex * 6 * M_2_PI
        let radius = normalizedIndex * Double(fieldWidth) / 2
        
        let centerX = Double(fieldWidth) * 0.5
        let centerY = Double(fieldHeight) * 0.5
        
        let x = cos(theta) * radius
        let y = sin(theta) * radius
        
        return CGPoint(x: CGFloat(centerX + x), y: CGFloat(centerY + y))
    }
    
}
