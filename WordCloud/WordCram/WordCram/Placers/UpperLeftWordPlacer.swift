//  UpperLeftWordPlacer.swift

import UIKit

public class UpperLeftWordPlacer: WordPlacer {
    
    private var g = GaussianGenerator()
    
    public func place(word: WCWord, wordIndex: Int, wordsCount: Int, wordImageWidth: Int, wordImageHeight: Int, fieldWidth: Int, fieldHeight: Int) -> CGPoint {
        let x = getOneUnder(limit: fieldWidth - wordImageWidth)
        let y = getOneUnder(limit: fieldHeight - wordImageHeight)
        return CGPoint(x: x, y: y)
    }
    
    private func getOneUnder(limit: Int) -> Int {
        let value = random(limit: random(limit: random(limit: random(limit: random(limit: 1)))))
        let map = PApplet.map(value: value, start1: 0, stop1: 1, start2: 0, stop2: Double(limit))
        return Int(round(map))
    }
    
    private func random(limit: Double) -> Double {
        return Double.random() * limit
    }
    
}
