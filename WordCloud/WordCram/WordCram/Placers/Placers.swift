//  Placers.swift

import UIKit

public class Placers {
    
    class HorizLineWordPlacer: WordPlacer {
        private var g = GaussianGenerator()
        
        func place(word: WCWord, wordIndex: Int, wordsCount: Int, wordImageWidth: Int, wordImageHeight: Int, fieldWidth: Int, fieldHeight: Int) -> CGPoint {
            let centerHorizLine = Int(Double(fieldHeight - wordImageHeight) * 0.5)
            let centerVertLine = Int(Double(fieldWidth - wordImageWidth) * 0.5)
            
            let xOff = g.next()! * Double(fieldWidth - wordImageWidth) * 0.2
            let yOff = g.next()! * 50
            
            return CGPoint(x: Double(centerVertLine) + xOff, y: Double(centerHorizLine) + yOff)
        }
    }
    
    class CenterClumpWordPlacer: WordPlacer {
        private var g = GaussianGenerator()
        private var stdev = 0.4
        
        func place(word: WCWord, wordIndex: Int, wordsCount: Int, wordImageWidth: Int, wordImageHeight: Int, fieldWidth: Int, fieldHeight: Int) -> CGPoint {
            return CGPoint(x: getOneUnder(upperLimit: Double(fieldWidth - wordImageWidth)), y: getOneUnder(upperLimit: Double(fieldHeight - wordImageHeight)))
        }
        
        func getOneUnder(upperLimit: Double) -> Int {
            let lim = PApplet.map(value: g.next()! * stdev, start1: -2, stop1: 2, start2: 0, stop2: upperLimit)
            return Int(round(lim))
        }
    }
    
    class HorizBandAnchoredLeftWordPlacer: WordPlacer {
        private var g = GaussianGenerator()
        
        func place(word: WCWord, wordIndex: Int, wordsCount: Int, wordImageWidth: Int, wordImageHeight: Int, fieldWidth: Int, fieldHeight: Int) -> CGPoint {
            let x = (1 - word.weight) * Double(fieldWidth) * g.next()!
            let y = Double(fieldHeight) * 0.5
            return CGPoint(x: x, y: y)
        }
    }
    
    
    public class func horizLine() -> WordPlacer {
        return HorizLineWordPlacer()
    }
    
    public class func centerClump() -> WordPlacer {
        return CenterClumpWordPlacer()
    }
    
    public class func horizBandAnchoredLeft() -> WordPlacer {
        return HorizBandAnchoredLeftWordPlacer()
    }
    
    public class func swirl() -> WordPlacer {
        return SwirlWordPlacer()
    }
    
}
