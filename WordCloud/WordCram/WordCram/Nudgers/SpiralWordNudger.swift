//  SpiralWordNudger.swift

import UIKit

public class SpiralWordNudger: WordNudger {
    
    private var thetaIncrement = Double.pi * 0.03
    
    public init() {}
    
    public func nudgeFor(word: WCWord, attemptNumber: Int) -> CGPoint {
        let rad = powerMap(power: 0.6, v: Double(attemptNumber), min1: 0, max1: 600, min2: 1, max2: 100)
        
        thetaIncrement = powerMap(power: 1, v: Double(attemptNumber), min1: 0, max1: 600, min2: 0.5, max2: 0.3)
        let theta = thetaIncrement * Double(attemptNumber)
        let x = cos(theta) * rad
        let y = sin(theta) * rad
        return CGPoint(x: x, y: y)
    }
    
    public func powerMap(power: Double, v: Double, min1: Double, max1: Double, min2: Double, max2: Double) -> Double {
        var val = PApplet.norm(value: v, start: min1, stop: max1)
        val = pow(val, power)
        return PApplet.lerp(start: min2, stop: max2, amt: val)
    }
    
}
