//  RandomWordNudger.swift

import UIKit

public class RandomWordNudger: WordNudger {
    
    private var g = GaussianGenerator()
    private var stdDev: Double
    
    public init(stdDev: Double) {
        self.stdDev = stdDev
    }
    
    public convenience init() {
        self.init(stdDev: 0.8)
    }
    
    public func nudgeFor(word: WCWord, attemptNumber: Int) -> CGPoint {
        return CGPoint(x: next(attempt: attemptNumber), y: next(attempt: attemptNumber))
    }
    
    private func next(attempt: Int) -> Double {
       return g.next()! * Double(attempt)
    }
    
}
