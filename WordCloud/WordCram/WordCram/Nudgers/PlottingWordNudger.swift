//  PlottingWordNudger.swift

import UIKit

public class PlottingWordNudger: WordNudger {
    
    private var context: CGContext
    private var wrapperNudger: WordNudger
    
    public init(context: CGContext, wrapperNudger: WordNudger) {
        self.context = context
        self.wrapperNudger = wrapperNudger
    }
    
    public func nudgeFor(word: WCWord, attemptNumber: Int) -> CGPoint {
        let v = wrapperNudger.nudgeFor(word: word, attemptNumber: attemptNumber)
        
        context.saveGState()
        context.setStrokeColor(UIColor.clear.cgColor)
        
        context.setFillColor(UIColor(hue: 40, saturation: 255, brightness: 255, alpha: 1).cgColor)
        
        let wordLoc = v + word.targetPlace
        let rect = CGRect(x: wordLoc.x, y: wordLoc.y, width: 3, height: 3)
        context.fillEllipse(in: rect)
        context.restoreGState()
        return v
    }
    
}
