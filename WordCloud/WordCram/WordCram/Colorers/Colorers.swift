//  Colorers.swift

import UIKit

public class Colorers {

    class PickFromColorer: WordColorer {
        private var colors: [UIColor]
        
        init(_ colors: [UIColor]) {
            self.colors = colors
        }
        
        func colorFor(word: WCWord) -> UIColor {
            let ind = Int(arc4random_uniform(UInt32(colors.count)))
            return colors[ind]
        }
    }
    
    class AlwaysUseColorer: WordColorer {
        private var color: UIColor
        
        init(_ color: UIColor) {
            self.color = color
        }
        
        func colorFor(word: WCWord) -> UIColor {
            return color
        }
    }
    

    public class func pickFrom(colors: UIColor...) -> WordColorer {
        return PickFromColorer(colors)
    }
    
    public class func pickFrom(colors: [UIColor]) -> WordColorer {
        return pickFrom(colors: colors)
    }
    
    public class func alwaysUse(color: UIColor) -> WordColorer {
        return AlwaysUseColorer(color)
    }
    
}
