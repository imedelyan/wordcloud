//  Fonters.swift

import UIKit

public class Fonters {

    class PickFromWordFonter: WordFonter {
        var fonts: [UIFont]
        
        init(_ fonts: [UIFont]) {
            self.fonts = fonts
        }
        
        func fontFor(word: WCWord) -> UIFont {
            let ind = Int(arc4random_uniform(UInt32(fonts.count)))
            return fonts[ind]
        }
    }
    
    class AlwaysUseWordFonter: WordFonter {
        var font: UIFont
        
        init(_ font: UIFont) {
            self.font = font
        }
        
        func fontFor(word: WCWord) -> UIFont {
            return font
        }
    }
    
    
    public class func pickFrom(fonts: UIFont...) -> WordFonter {
        return PickFromWordFonter(fonts)
    }
    
    public class func pickFrom(fonts: [UIFont]) -> WordFonter {
        return Fonters.pickFrom(fonts: fonts)
    }
    
    public class func alwaysUse(font: UIFont) -> WordFonter {
        return AlwaysUseWordFonter(font)
    }
    
}
