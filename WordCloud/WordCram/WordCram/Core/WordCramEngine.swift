//  WordCramEngine.swift

import UIKit

class WordCramEngine {
    
    private var renderer: WordRenderer
    
    private var fonter: WordFonter
    private var sizer: WordSizer
    private var colorer: WordColorer
    private var angler: WordAngler
    private var placer: WordPlacer
    private var nudger: WordNudger
    
    private var eWords: [EngineWord]!
    private var eWordIter: IndexingIterator<[EngineWord]>!
    private var drawnWords = [EngineWord]()
    internal private(set) var skippedWords = [WCWord]()
    
    private var renderOptions: RenderOptions
    private var observer: Observer
    
    init(renderer: WordRenderer, words: [WCWord], fonter: WordFonter, sizer: WordSizer, colorer: WordColorer, angler: WordAngler, placer: WordPlacer, nudger: WordNudger, shaper: WordShaper, bbTreeBuilder: BBTreeBuilder, renderOptions: RenderOptions, observer: Observer) {
        self.renderer = renderer

        self.fonter = fonter
        self.sizer = sizer
        self.colorer = colorer
        self.angler = angler
        self.placer = placer
        self.nudger = nudger
        self.observer = observer
        
        self.renderOptions = renderOptions
        self.eWords = wordsIntoEngineWords(words: words, wordShaper: shaper, bbTreeBuilder: bbTreeBuilder)
//        self.eWordIter = eWords.generate()
    }
    
    private func wordsIntoEngineWords(words: [WCWord], wordShaper: WordShaper, bbTreeBuilder: BBTreeBuilder) -> [EngineWord] {
        var engineWords = [EngineWord]()
        
        var maxNumberOfWords = words.count
        if renderOptions.maxNumberOfWordsToDraw >= 0 {
            maxNumberOfWords = min(maxNumberOfWords, renderOptions.maxNumberOfWordsToDraw)
        }
        
        for i in 0..<maxNumberOfWords {
            let word = words[i]
            let eWord = EngineWord(word: word, rank: i, wordCount: words.count, bbTreeBuilder: bbTreeBuilder)
            
            let wordFont = word.getFont(fonter: fonter)
            let wordSize = word.getSize(sizer: sizer, rank: i, wordCount: words.count)
            let wordAngle = word.getAngle(angler: angler)
            
            let shape = wordShaper.getShapeFor(word: eWord.word.word, font: wordFont, fontSize: CGFloat(wordSize), angle: wordAngle)
            if isTooSmall(shape: shape, minShapeSize: renderOptions.minShapeSize) {
                skipWord(word: word, reason: .ShapeWasTooSmall)
            } else {
                eWord.setShape(shape: shape, swelling: renderOptions.wordPadding)
                engineWords.append(eWord) // DON'T add eWords with no shape
            }
        }
        
        for i in maxNumberOfWords..<words.count {
            skipWord(word: words[i], reason: .WasOverMaxNumberOfWords)
        }
        
        return engineWords
    }
    
    private func isTooSmall(shape: CGRect, minShapeSize: Int) -> Bool {
        let minSize = CGFloat(minShapeSize < 1 ? 1 : minShapeSize)
        return shape.height < minSize || shape.width < minSize
    }
    
    private func skipWord(word: WCWord, reason: WordSkipReason) {
        word.skippedBecause = reason
        skippedWords.append(word)
        observer.wordSkipped(word: word)
    }
    
    internal func drawAll() {
        observer.beginDraw()
        while drawNext() {}
        renderer.finish()
        observer.endDraw()
    }
    
    internal func drawNext() -> Bool {
        for eWord in eWords {
            let wasPlaced = placeWord(eWord: eWord)
            if wasPlaced {
                drawWordImage(word: eWord)
                observer.wordDrawn(word: eWord.word)
            }
//            return true
        }
        return false
    }
    
//    internal func drawNext() -> Bool {
//        if let eWord = eWordIter.next() {
//            let wasPlaced = placeWord(eWord: eWord)
//            if wasPlaced {
//                drawWordImage(word: eWord)
//                observer.wordDrawn(word: eWord.word)
//            }
//
//            return true
//        }
//        return false
//    }
    
    private func placeWord(eWord: EngineWord) -> Bool {
        let word = eWord.word
        let rect = eWord.shape
        let wordImageWidth = Int(rect!.width)
        let wordImageHeight = Int(rect!.height)
        
        eWord.setDesiredLocation(placer: placer, count: eWords.count, wordImageWidth: wordImageWidth, wordImageHeight: wordImageHeight, fieldWidth: renderer.getWidth(), fieldHeight: renderer.getHeight())
        
        // Set maximum number of placement trials
        let maxAttemptsToPlace = renderOptions.maxAttemptsToPlaceWord > 0
            ? renderOptions.maxAttemptsToPlaceWord
            : calculateMaxAttemptsFromWordWeight(word: word!)
        
        var lastCollidedWith: EngineWord?
        for attempt in 0..<maxAttemptsToPlace {
            eWord.nudge(nudge: nudger.nudgeFor(word: word!, attemptNumber: attempt))
            
            let loc = eWord.currentLocation
            if loc!.x < 0 || loc!.y < 0
                || loc!.x + CGFloat(wordImageWidth) >= CGFloat(renderer.getWidth())
                || loc!.y + CGFloat(wordImageHeight) >= CGFloat(renderer.getHeight()) {
                continue
            }
            
            if lastCollidedWith != nil && eWord.overlaps(other: lastCollidedWith!) {
                continue
            }
            
            var foundOverlap = false
            for otherWord in drawnWords {
                if eWord.overlaps(other: otherWord) {
                    foundOverlap = true
                    lastCollidedWith = otherWord
                }
            }
            
            if !foundOverlap {
                eWord.finalizeLocation()
                return true
            }
        }
        
        skipWord(word: eWord.word, reason: .NoSpace)
        return false
    }
    
    private func calculateMaxAttemptsFromWordWeight(word: WCWord) -> Int {
        return Int((1 - word.weight) * 600 + 500)
    }
    
    private func drawWordImage(word: EngineWord) {
        drawnWords.append(word)
        renderer.drawWord(word: word, color: word.word.getColor(colorer: colorer))
    }
    
    internal func getWordAt(x: Double, y: Double) -> WCWord? {
        return drawnWords
            .filter { $0.containsPoint(x: x, y: y) }
            .map { $0.word }.first
    }
    
    internal func getProgress() -> Double {
        return 0 // FIXME: not implemented
    }
    
}
