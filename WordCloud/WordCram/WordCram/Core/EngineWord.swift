//  EngineWord.swift

import UIKit

class EngineWord: NSObject {
    
    var word: WCWord!
    var rank: Int!
    
    var shape: CGRect!
    private var bbTreeBuilder: BBTreeBuilder!
    private var bbTree: BBTree!
    
    private var desiredLocation: CGPoint!
    var currentLocation: CGPoint!
    
    init(word: WCWord, rank: Int, wordCount: Int, bbTreeBuilder: BBTreeBuilder) {
        super.init()
        self.word = word
        self.rank = rank
        self.bbTreeBuilder = bbTreeBuilder
        word.engineWord = self
    }
    
    func setShape(shape: CGRect, swelling: Int) {
        self.shape = shape
        bbTree = bbTreeBuilder.makeTree(shape: shape, swelling: swelling)
    }
    
    func overlaps(other: EngineWord) -> Bool {
        return bbTree.overlaps(otherTree: other.bbTree)
    }
    
    func containsPoint(x: Double, y: Double) -> Bool {
        return bbTree.containsPoint(x: x, y: y)
    }
    
    func setDesiredLocation(placer: WordPlacer, count: Int, wordImageWidth: Int, wordImageHeight: Int, fieldWidth: Int, fieldHeight: Int) {
        desiredLocation = word.getTargetPlace(placer: placer, rank: rank, count: count, wordImageWidth: wordImageWidth, wordImageHeight: wordImageHeight, fieldWidth: fieldWidth, fieldHeight: fieldHeight)
        currentLocation = desiredLocation
    }
    
    func nudge(nudge: CGPoint) {
        currentLocation = desiredLocation + nudge
        bbTree.setLocation(left: Int(currentLocation.x), top: Int(currentLocation.y))
    }
    
    func finalizeLocation() {
        let transform = CGAffineTransform(translationX: currentLocation.x, y: currentLocation.y)
        shape = shape.applying(transform)
        bbTree.setLocation(left: Int(currentLocation.x), top: Int(currentLocation.y))
        word.renderedPlace = currentLocation
    }
    
    func wasPlaced() -> Bool {
        return word.wasPlaced()
    }
    
    func wasSkipped() -> Bool {
        return word.wasSkipped()
    }
    
}
