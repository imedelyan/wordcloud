//  WCWord.swift

import UIKit

public class WCWord: NSObject, Comparable {
    
    public var word: String
    public var weight: Double
    
    public private(set) var presetSize: Double!
    public private(set) var presetAngle: Double!
    public private(set) var presetFont: UIFont!
    public private(set) var presetColor: UIColor!
    public private(set) var presetTargetPlace: CGPoint!
    
    public private(set) var renderedSize: Double!
    public private(set) var renderedAngle: Double!
    public private(set) var renderedFont: UIFont!
    public private(set) var renderedColor: UIColor!
    public private(set) var targetPlace: CGPoint!
    var renderedPlace: CGPoint!
    public var skippedBecause: WordSkipReason!
    
    private var properties = [String: AnyObject]()
    
    public init(word: String, weight: Double) {
        self.word = word
        self.weight = weight
    }
    
    public func setSize(size: Double) -> WCWord {
        self.presetSize = size
        return self
    }
    
    public func setAngle(angle: Double) -> WCWord {
        self.presetAngle = angle
        return self
    }
    
    public func setFont(font: UIFont) -> WCWord {
        self.presetFont = font
        return self
    }
    
    public func setColor(color: UIColor) -> WCWord {
        self.presetColor = color
        return self
    }

    public func setPlace(place: CGPoint) -> WCWord {
        self.presetTargetPlace = place
        return self
    }
    
    public func setPlace(x: Double, y: Double) -> WCWord {
        presetTargetPlace = CGPoint(x: x, y: y)
        return self
    }
    
    // These methods are called by EngineWord: they return (for instance) 
    // either the color the user set, or the value returned
    // by the WordColorer. They're package-local, so they can't be called 
    // by the sketch.
    
    func getSize(sizer: WordSizer, rank: Int, wordCount: Int) -> Double {
        renderedSize = presetSize ?? sizer.sizeFor(word: self, wordRank: rank, wordCount: wordCount)
        return renderedSize
    }
    
    func getAngle(angler: WordAngler) -> Double {
        renderedAngle = presetAngle ?? angler.angleFor(word: self)
        return renderedAngle
    }
    
    func getFont(fonter: WordFonter) -> UIFont {
        renderedFont = presetFont ?? fonter.fontFor(word: self)
        return renderedFont
    }
    
    func getColor(colorer: WordColorer) -> UIColor {
        renderedColor = presetColor ?? colorer.colorFor(word: self)
        return renderedColor
    }
    
    func getTargetPlace(placer: WordPlacer, rank: Int, count: Int, wordImageWidth: Int, wordImageHeight: Int, fieldWidth: Int, fieldHeight: Int) -> CGPoint {
        targetPlace = presetTargetPlace ?? placer.place(word: self, wordIndex: rank, wordsCount: count, wordImageWidth: wordImageWidth, wordImageHeight: wordImageHeight, fieldWidth: fieldWidth, fieldHeight: fieldHeight)
        return targetPlace
    }
    
    public func wasPlaced() -> Bool {
        return renderedPlace != nil
    }
    
    public func wasSkipped() -> Bool {
        return skippedBecause != nil
    }
    
    public subscript(propertyName: String) -> AnyObject? {
        get { return properties[propertyName] }
        set(newValue) { properties[propertyName] = newValue }
    }
    
    // Note: these are only so we can delegate to EngineWord for getShape()
    var engineWord: EngineWord?
    
    public var renderedWidth: CGFloat {
        return engineWord?.shape.width ?? 0
    }
    
    public var renderedHeight: CGFloat {
        return engineWord?.shape.height ?? 0
    }
    
    public override var description: String {
        var status = ""
        if wasPlaced() {
            status = "\(renderedPlace.x),\(renderedPlace.y)"
        } else if wasSkipped() {
            status = skippedBecause.rawValue
        }
        if status.lengthOfBytes(using: String.Encoding.utf8) != 0 {
            status = "[\(status)]"
        }
        return "\(word) (\(weight)) \(status)"
    }

    
}


public func ==(lhs: WCWord, rhs: WCWord) -> Bool {
    return lhs.weight == rhs.weight
}

public func <(lhs: WCWord, rhs: WCWord) -> Bool {
    return lhs.weight < rhs.weight
}
