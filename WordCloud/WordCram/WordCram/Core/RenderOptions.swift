//  RenderOptions.swift

import Foundation

class RenderOptions {
    var maxAttemptsToPlaceWord = -1
    var maxNumberOfWordsToDraw = -1
    var minShapeSize = 4
    var wordPadding = 1
    
    var rightToLeft = false
}
