//  WordCram.swift

import UIKit

public class WordCram {
    
    public private(set) var words: [WCWord]!
    private var textSources = [TextSource]()
    private var extraStopWords = ""
    private var excludeNumbers = true
    private enum TextCase { case Lower, Upper, Keep }
    private var textCase: TextCase = .Keep
    
    private var wordCramEngine: WordCramEngine?
    
    private var ctx: CGContext!
    
    private var fonter: WordFonter!
    private var sizer: WordSizer!
    private var colorer: WordColorer!
    private var angler: WordAngler!
    private var placer: WordPlacer!
    private var nudger: WordNudger!
    
    private var renderer: WordRenderer!
    private var renderOptions = RenderOptions()
    private var observer: Observer!
    
    public init(ctx: CGContext) {
        self.ctx = ctx
        
        ctx.translateBy(x: 0, y: CGFloat(Double(ctx.height) / Double(UIScreen.main.scale)))
        ctx.scaleBy(x: 1, y: -1)
        
        renderer = ProcessingWordRenderer(context: ctx)
        observer = SketchCallbackObserver(delegate: nil)
    }
    
    public func withStopWords(extraStopWords: String) -> WordCram {
        self.extraStopWords = extraStopWords
        return self
    }
    
    public func excludeNumbers_() -> WordCram {
        excludeNumbers = true
        return self
    }
    
    public func includeNumbers() -> WordCram {
        excludeNumbers = false
        return self
    }
    
    public func lowerCase() -> WordCram {
        textCase = .Lower
        return self
    }
    
    public func upperCase() -> WordCram {
        textCase = .Upper
        return self
    }
    
    public func keepCase() -> WordCram {
        textCase = .Keep
        return self
    }
    
    public func fromWebPage(webPageAddress: String) -> WordCram {
        return fromWebPage(webPageAddress: webPageAddress, cssSelector: nil)
    }
    
    public func fromWebPage(webPageAddress: String, cssSelector: String?) -> WordCram {
        return fromText(textSource: WebPage(url: webPageAddress, cssSelector: cssSelector))
        // TODO: add parent
    }
    
    public func fromHTMLFile(htmlFilePath: String) -> WordCram {
        return fromHTMLFile(htmlFilePath: htmlFilePath, cssSelector: nil)
    }
    
    public func fromHTMLFile(htmlFilePath: String, cssSelector: String?) -> WordCram {
        return fromText(textSource: WebPage(url: htmlFilePath, cssSelector: cssSelector))
        // TODO: add parent
    }
    
    public func fromTextFile(textFilePathOrUrl: String) -> WordCram {
        return fromText(textSource: TextFile(path: textFilePathOrUrl))
    }
    
    public func fromTextString(text: String...) -> WordCram {
        return fromText(textSource: Text(text.joined(separator: " ")))
    }
    
    public func fromText(textSource: TextSource) -> WordCram {
        textSources.append(textSource)
        return self
    }
    
    public func fromWords(words: [WCWord]) -> WordCram {
        self.words = words
        return self
    }
    
    // MARK: - //
    
    public func withFonts(fontNames: String...) -> WordCram {
        let fonts = fontNames.map { UIFont(name: $0, size: 1) }.filter { $0 != nil }.map { $0! }
        return withFonts(fonts: fonts)
    }
    
    public func withFont(fontName: String) -> WordCram {
        let font = UIFont(name: fontName, size: 1)!
        return withFont(font: font)
    }
    
    public func withFonts(fonts: UIFont...) -> WordCram {
        return withFonter(fonter: Fonters.pickFrom(fonts: fonts))
    }
    
    public func withFonts(fonts: [UIFont]) -> WordCram {
        return withFonts(fonts: fonts)
    }
    
    public func withFont(font: UIFont) -> WordCram {
        return withFonter(fonter: Fonters.pickFrom(fonts: font))
    }
    
    public func withFonter(fonter: WordFonter) -> WordCram {
        self.fonter = fonter
        return self
    }
    
    public func sizedByWeight(minSize: Int, maxSize: Int) -> WordCram {
        return withSizer(sizer: Sizers.byWeight(minSize: minSize, maxSize: maxSize))
    }
    
    public func sizedByRank(minSize: Int, maxSize: Int) -> WordCram {
        return withSizer(sizer: Sizers.byRank(minSize: minSize, maxSize: maxSize))
    }
    
    public func withSizer(sizer: WordSizer) -> WordCram {
        self.sizer = sizer
        return self
    }
    
    public func withColors(colors: UIColor...) -> WordCram {
        return withColorer(colorer: Colorers.pickFrom(colors: colors))
    }
    
    public func withColor(color: UIColor) -> WordCram {
        return withColors(colors: color)
    }
    
    public func withColorer(colorer: WordColorer) -> WordCram {
        self.colorer = colorer
        return self
    }
    
    public func angledAt(anglesInRadians: Double...) -> WordCram {
        return withAngler(angler: Anglers.pickFrom(angles: anglesInRadians))
    }
    
    public func angledBetween(minAngleInRadians: Double, _ maxAngleInRadians: Double) -> WordCram {
        return withAngler(angler: Anglers.randomBetween(minAngleInRadians, maxAngleInRadians))
    }
    
    public func withAngler(angler: WordAngler) -> WordCram {
        self.angler = angler
        return self
    }
    
    public func withPlacer(placer: WordPlacer) -> WordCram {
        self.placer = placer
        return self
    }
    
    public func withNudger(nudger: WordNudger) -> WordCram {
        self.nudger = nudger
        return self
    }
    
    public func maxAttemptsToPlaceWord(maxAttempts: Int) -> WordCram {
        renderOptions.maxAttemptsToPlaceWord = maxAttempts
        return self
    }
    
    public func maxNumberOfWordsToDraw(maxWords: Int) -> WordCram {
        renderOptions.maxNumberOfWordsToDraw = maxWords
        return self
    }
    
    public func minShapeSize(minShapeSize: Int) -> WordCram {
        renderOptions.minShapeSize = minShapeSize
        return self
    }
    
//    public func toCanvas(canvas: PGraphics) -> WordCram {

    public func withWordPadding(padding: Int) -> WordCram {
        renderOptions.wordPadding = padding
        return self
    }
    
    public func testPlacer() {
        initComponents()
        let shaper = WordShaper(context: ctx, rightToLeft: renderOptions.rightToLeft)
        let heatMap = PlacerHeatMap(words: words, fonter: fonter, sizer: sizer, angler: angler, placer: placer, nudger: nudger, shaper: shaper)
        heatMap.draw(context: ctx)
    }
    
    private func getWordCramEngine() -> WordCramEngine {
        if wordCramEngine == nil {
            initComponents()
            let shaper = WordShaper(context: ctx, rightToLeft: renderOptions.rightToLeft)
            wordCramEngine = WordCramEngine(renderer: renderer, words: words, fonter: fonter, sizer: sizer, colorer: colorer, angler: angler, placer: placer, nudger: nudger, shaper: shaper, bbTreeBuilder: BBTreeBuilder(), renderOptions: renderOptions, observer: observer)
        }
        return wordCramEngine!
    }
    
    private func initComponents() {
        if words == nil && !textSources.isEmpty {
            var text = joinTextSources()
            
            text = textCase == .Lower ? text.lowercased()
                : textCase == .Upper ? text.uppercased()
                : text
            
            words = WordCounter()
                .withExtraStopWords(extraStopWordsString: extraStopWords)
                .shouldExcludeNumbers(shouldExcludeNumbers: excludeNumbers)
                .count(text: text, renderOptions: renderOptions)
            observer.wordsCounted(words: words)
            if words.count == 0 {
                warnScripterAboutEmptyWordArray()
            }
        }
        words = WordSorterAndScaler().sortAndScale(rawWords: words)
        
        if fonter == nil { fonter = Fonters.alwaysUse(font: UIFont.systemFont(ofSize: 1)) }
        if sizer == nil { sizer = Sizers.byWeight(minSize: 5, maxSize: 70) }
        if colorer == nil { colorer = Colorers.alwaysUse(color: UIColor.black) }
        if angler == nil { angler = Anglers.mostlyHoriz() }
        if placer == nil { placer = Placers.horizLine() }
        if nudger == nil { nudger = SpiralWordNudger() }
    }
    
    private func joinTextSources() -> String {
        return textSources.reduce("") { acc, x in return acc + x.getText() + "\n" }
    }
    
    private func warnScripterAboutEmptyWordArray() {
        print("cue.language cant' find any non-stop words in your text. This could be because your file encoding is wrong, or because all your words are single characters, among other things.")
    }
    
    
    public func hasMore() -> Bool {
        return false // FIXME: not implemented
    }
    
    public func drawNext() {
        getWordCramEngine().drawNext()
    }
    
    public func drawAll() {
        getWordCramEngine().drawAll()
    }
    
//    public func getWords() -> [WCWord] {
//        return words
//    }
    
    public func getWordAt(x: Double, y: Double) -> WCWord? {
        return getWordCramEngine().getWordAt(x: x, y: y)
    }
    
    public func getSkippedWords() -> [WCWord] {
        return getWordCramEngine().skippedWords
    }
    
    public func withObserver(observer: Observer) -> WordCram {
        self.observer = observer
        return self
    }
    
}
