//  StopWords.swift

import Foundation

public enum StopWords {
    
    case Arabic, Armenian, Catalan, Croatian, Czech, Dutch
    
    private static var values = [Arabic, Armenian, Catalan, Croatian, Czech, Dutch]
    
//    public static func guess(text: String) -> StopWords {
//        return guess(Counter<String>
//    }
    
    public static func guess(wordCounter: Counter<String>) -> StopWords {
        return guess(words: wordCounter.getMostFrequent(n: 50))
    }
    
    public static func guess(words: [String]) -> StopWords {
        var currentWinner: StopWords?
//        for _ in StopWords.values {
///           let count = words.filter { stopWord.
//        }
        return currentWinner!
    }
    
    public func isStopWord(s: String) -> Bool {
        if s.lengthOfBytes(using: String.Encoding.utf8) == 1 {
            return true
        }

        return false
    }
    
}
