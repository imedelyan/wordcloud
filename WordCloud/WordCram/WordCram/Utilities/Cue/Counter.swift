//  Counter.swift

import Foundation

public class Counter<T> where T: Hashable, T: Comparable {
    
    private var items = [T: Int]()
    public private(set) var totalItemsCount = 0
    
    public init() {}
    
    public convenience init(items: [T]) {
        self.init()
        noteAll(items: items)
    }
    
    public func noteAll(items: [T]) {
        items.map { t -> Void in self.note(item: t, count: 1) }
    }
    
    public func note(item: T) {
        note(item: item, count: 1)
    }
    
    public func note(item: T, count: Int) {
        if let existingCount = items[item] {
            items[item] = existingCount + count
        } else {
            items[item] = count
        }
        totalItemsCount += count
    }
    
    public func merge(c: Counter<T>) {
        for (k, v) in c.items {
            note(item: k, count: v)
        }
    }
    
    public func getMostFrequent(n: Int) -> [T] {
        let all = getAllByFrequency()
        let resultSize = min(n, items.count)
        return Array(all[0..<resultSize]).map { $0.0 }
    }
    
    public func getAllByFrequency() -> [(T, Int)] {
        let all = Array(items.keys).map { ($0, self.items[$0]!) }
        return all.sorted(by: { $0.0 > $1.0 })
    }
    
    public func getCount(item: T) -> Int {
        return items[item] ?? 0
    }
    
    public func clear() {
        items.removeAll(keepingCapacity: false)
    }
    
    // TODO: replace list with set
    public func entrySet() -> [(T, Int)] {
        return Array(items.keys).map { ($0, self.items[$0]!) }
    }
    
//    public func keySet() {
//        
//    }
    
    public func keyList() -> [T] {
        return getMostFrequent(n: items.count)
    }
    
}
