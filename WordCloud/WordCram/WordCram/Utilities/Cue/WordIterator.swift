//  WordIterator.swift

import Foundation

public struct WordIterator: IteratorProtocol {
    
    private struct Static {
        static let Letter = "[@+\\w]"
        static let Joiner = "[-.:/'`\\p{M}\\u2032\\u00A0\\u200C\\u200D~]"
        static let WCWord = "\(Letter)+(\(Joiner)+\(Letter)+)*"
    }
    
    private var text: String
    private var generator: RegexMatchCaptureGenerator
    
    public init(text: String) {
        self.text = text
        generator = (text =~ Static.WCWord).generate()
    }
    
    public mutating func next() -> String? {
        return generator.next()
    }

}
