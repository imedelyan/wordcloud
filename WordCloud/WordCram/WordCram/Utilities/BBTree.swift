//  BBTree.swift

import UIKit

public class BBTree {
    
    private var left: Int
    private var top: Int
    private var right: Int
    private var bottom: Int
    private var kids: [BBTree]?
    
    private var parent: BBTree!
    private var rootX: Int!
    private var rootY: Int!
    
    private var swelling = 0
    
    public init(left: Int, top: Int, right: Int, bottom: Int) {
        self.left = left
        self.top = top
        self.right = right
        self.bottom = bottom
    }
    
    func addKids(kids: BBTree?...) {
        self.kids = kids.reduce([]) {
            (kidList: [BBTree], kid: BBTree?) -> [BBTree] in
            if kid == nil {
                return kidList
            }
            var newList = kidList
            newList.append(kid!)
            kid!.parent = self
            return newList
        }
    }
    
    public func setLocation(left: Int, top: Int) {
        rootX = left
        rootY = top
    }
    
    private func getRoot() -> BBTree {
        return parent == nil ? self : parent.getRoot()
    }
    
    public func overlaps(otherTree: BBTree) -> Bool {
        if rectCollide(aTree: self, bTree: otherTree) {
            if isLeaf() && otherTree.isLeaf() {
                return true
            }
        } else if isLeaf() { // Then otherTree isn't a leaf.
            if let otherKids = otherTree.kids {
                for otherKid in otherKids {
                    if overlaps(otherTree: otherKid) {
                        return true
                    }
                }
            } else {
                return false
            }
        }
        else {
            if let kids = kids { 
                for myKid in kids {
                    if otherTree.overlaps(otherTree: myKid) {
                        return true
                    }
                }
            }
        }
        return false
    }
    
    private func rectCollide(aTree: BBTree, bTree: BBTree) -> Bool {
        let a = aTree.getPoints()
        let b = bTree.getPoints()
        
        return a[3] > b[1] && a[1] < b[3] && a[2] > b[0] && a[0] < b[2]
    }
    
    private func getPoints() -> [Int] {
        let root = getRoot()
        return [
            root.rootX - swelling + left,
            root.rootY - swelling + top,
            root.rootX + swelling + right,
            root.rootY + swelling + bottom
        ]
    }
    
    func containsPoint(x: Double, y: Double) -> Bool {
        let root = getRoot()
        return Double(root.rootX + left) < x &&
            Double(root.rootX + right) > x &&
            Double(root.rootY + top) < y &&
            Double(root.rootY + bottom) > y
    }
    
    func isLeaf() -> Bool {
        return kids == nil
    }
    
    func swell(extra: Int) {
        swelling += extra
        if !isLeaf() {
            kids?.map { $0.swell(extra: extra) }
        }
    }
    
    public func draw(ctx: CGContext) {
        ctx.saveGState()
        ctx.setFillColor(UIColor.clear.cgColor.components!)
        
        let strokeColor = UIColor(red: 30/255, green: 255/255, blue: 255/255, alpha: 80/255).cgColor
        ctx.setStrokeColor(strokeColor.components!)
        drawLeaves(ctx: ctx)
        
        ctx.restoreGState()
    }
    
    public func drawLeaves(ctx: CGContext) {
        if isLeaf() {
            drawBounds(ctx: ctx, points: getPoints())
        } else {
            kids?.map { $0.drawLeaves(ctx: ctx) }
        }
    }
    
    private func drawBounds(ctx: CGContext, points: [Int]) {
        let rect = CGRect(x: points[0], y: points[1], width: points[2], height: points[3])
        ctx.stroke(rect)
    }
    
}
