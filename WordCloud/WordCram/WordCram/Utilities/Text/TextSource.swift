//  TextSource.swift

import Foundation

public protocol TextSource {
    func getText() -> String
}
