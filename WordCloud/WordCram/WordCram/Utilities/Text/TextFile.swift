//  TextFile.swift

import UIKit

public class TextFile: TextSource {

    private var path: String
    
    public init(path: String) {
        self.path = path
    }
    
    public func getText() -> String {
        let path = Bundle.main.path(forResource: self.path, ofType: nil)
        do{
            let text = try String(contentsOfFile: path!, encoding: String.Encoding.utf8)
            return text
        }
        catch
        {
            return ""
        }
    }
    
}
