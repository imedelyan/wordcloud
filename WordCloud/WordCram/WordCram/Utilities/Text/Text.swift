//  Text.swift

import Foundation

public class Text: TextSource {
    
    private var text: String
    
    public init(_ text: String) {
        self.text = text
    }
    
    public func getText() -> String {
        return text
    }
    
}
