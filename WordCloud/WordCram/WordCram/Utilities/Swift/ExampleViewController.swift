//  ExampleViewController.swift

import UIKit

class ExampleViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        UIGraphicsBeginImageContext(imageView.bounds.size)
//        UIImage().drawInRect(imageView.bounds)
//        imageView.image = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
    }
    
    @IBAction func buttonTapped(sender: AnyObject) {
        let wordArray = [
            WCWord(word: "Hello", weight: 100),
            WCWord(word: "WordCram", weight: 60)
        ]
        
        let words = [("one", 150), ("two", 120), ("three", 110), ("awesome", 300), ("something", 250)]
        let wA = words.map { (k: String, v: Int) -> WCWord in WCWord(word: k, weight: Double(v)) }

//        let size = view.bounds.size
        let size = CGSize(width: 500, height: 500)
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        let context = UIGraphicsGetCurrentContext()
        
        let wordCram = WordCram(ctx: context!)
//            .fromTextString(text)
//            .fromTextFile("text2")
            .fromWords(words: wA)
            
//            .withPlacer(Placers.centerClump())
//            .withPlacer(PlottingWordPlacer(context: context, wrapperPlacer: Placers.centerClump()))
//            .withNudger(PlottingWordNudger(context: context, wrapperNudger: SpiralWordNudger()))
            .withPlacer(placer: Placers.horizBandAnchoredLeft())
//            .withPlacer(Placers.horizLine())
//            .withPlacer(Placers.swirl())
            
//            .withWordPadding(1)
            
            .withSizer(sizer: Sizers.byWeight(minSize: 5, maxSize: 95))
//            .withSizer(Sizers.byRank(minSize: 5, maxSize: 70))

            .withPlacer(placer: PlottingWordPlacer(context: context!, wrapperPlacer: Placers.swirl()))
//            .withNudger(PlottingWordNudger(context: context, wrapperNudger: RandomWordNudger()))
            .withNudger(nudger: RandomWordNudger())
//            .withNudger(SpiralWordNudger())
            
            .withWordPadding(padding: 10)
        
            .maxNumberOfWordsToDraw(maxWords: 100)
            .minShapeSize(minShapeSize: 5)
            .lowerCase()
            .withStopWords(extraStopWords: "и в во не что он на я б с со как а то все она так его но да ты к у же вы за бы по только ее мне было вот от меня еще нет о из ему теперь когда даже ну вдруг ли если уже или ни быть был него до вас нибудь опять уж вам сказал ведь там потом себя ничего ей может они тут где есть надо ней для мы тебя их чем была сам чтоб без будто человек чего раз тоже себе под жизнь будет ж тогда кто этот говорил того потому этого какой совсем ним здесь этом один почти мой тем чтобы нее кажется сейчас были куда зачем сказать всех никогда сегодня можно при наконец два об другой хоть после над больше тот через эти нас про всего них какая много разве сказала три эту моя впрочем хорошо свою этой перед иногда лучше чуть том нельзя такой им более всегда конечно всю между без безо бишь благодаря близ более больше будем будет будете будешь будто буду будут бы бывало был была были было быть вам вами вас ваш ваша ваше вашего вашей вашем вашему вашею ваши вашим вашими ваших вашу вблизи вверх вверху ввиду вдоволь вдоль вдруг ведь вероятно весь весьма включая влево вместо вне вниз внизу внутри внутрь во вокруг вон вообще вообщем вопреки вот вполне вправо впрочем вроде все всегда всего всей всем всеми всему всех всею вслед вследствие всю всюду вся всё всём вы где да дабы давай даже дескать для до докуда другая другие другим другими других другого другое другой другом другому другою другую его едва ее ей емнип ему если есть еще ещё её же за запросто затем зато зачем здесь из извне изрядно или иль им именно иметь ими имхо иначе исключая итак их ихая ихие ихий ихим ихими ихих ихнего ихнее ихней ихнем ихнему ихнею ихние ихний ихним ихними ихних ихнюю ихняя как какая каких какую кем ко когда коли конечно которая которого которое которой котором которому которою которую которые который которым которыми которых кроме кстати кто куда ли либо лишь ль между менее меньше меня мне мной мною мну мог могла могли могло могу могут мое моего моей моем моему моею можем может можете можешь мои моим моими моих мой мол мою моя моё моём мы на навсегда навстречу над надо наконец нам нами наподобие наружу нас насколько насчет насчёт начиная наш наша наше нашего нашей нашем нашему нашею наши нашим нашими наших нашу не негде него незачем ней некем некогда некому некоторый некто некуда неоткуда несколько несмотря несомненно нет неужели нечего нечем нечто неё ни нибудь нигде никак никем никогда никого никому никто никуда ним ними ниоткуда нисколько них ничего ничем ничему ничто ничуть но ну об оба обе обеим обеими обеих обо обоим обоих однако около он она они оно опять от откуда отнюдь ото отовсюду отсюда оттого оттуда отчего очень перед по под подо подобно пока после потому походу почему почти поэтому при притом причем причём про прочая прочего прочее прочей прочем прочему прочею прочие прочий прочим прочими прочих прочую пускай пусть разве сам сама самая сами самим самими самих само самого самое самой самом самому саму самые самый сверх сколько слева следовательно слишком словно снова со собой собою справа спустя суть та так такая также такие таким такими таких такого такое такой таком такому такою такую там твое твоего твоей твоем твоему твоею твои твоим твоих твой твою твоя твоё твоём те тебе тебя тем теми тех то тобой тобою тогда того тоже тоими той только том тому тот тотчас тою ту тут ты уж уже хоть хотя чего чей чем через что чтобы чье чьего чьей чьем чьему чьею чьи чьим чьими чьих чью чья чьё чьём чём эта эти этим этими этих это этого этой этом этому этот этою эту в-пятых в-третьих в-четвертых в-четвёртых во-вторых во-первых едва-едва еле-еле из-за из-под как-никак по-моему по-над по-под то-есть точь-в-точь чуть-чуть т.д. т.е. т.к. т.н. т.п.")

        DispatchQueue.main.async {
            wordCram.drawAll()
/*
            let image = UIGraphicsGetImageFromCurrentImageContext().CGImage

            let width = CGImageGetWidth(image) / 2
            let height = CGImageGetHeight(image) / 2
            let bitsPerComponent = CGImageGetBitsPerComponent(image)
            let bytesPerRow = CGImageGetBytesPerRow(image)
            let colorSpace = CGImageGetColorSpace(image)
            let bitmapInfo = CGImageGetBitmapInfo(image)
            
            let context = CGBitmapContextCreate(nil, width, height, bitsPerComponent, bytesPerRow, colorSpace, bitmapInfo)
            
            CGContextSetInterpolationQuality(context, kCGInterpolationHigh)
            
            CGContextDrawImage(context, CGRect(origin: CGPointZero, size: CGSize(width: CGFloat(width), height: CGFloat(height))), image)
            
            let scaledImage = UIImage(CGImage: CGBitmapContextCreateImage(context))
            
            self.imageView.image = scaledImage
*/
            self.imageView.image = UIGraphicsGetImageFromCurrentImageContext()
            
            let fileManager = FileManager.default
            let imageData = UIGraphicsGetImageFromCurrentImageContext()!.pngData()
            fileManager.createFile(atPath: "/Users/alexander/Desktop/WordCram.png", contents: imageData, attributes: nil)
            
            UIGraphicsEndImageContext()
        }

    }

}
