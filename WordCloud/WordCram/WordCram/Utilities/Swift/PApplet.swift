//  PApplet.swift

import Foundation

public class PApplet {
    
    public final class func lerp(start: Double, stop: Double, amt: Double) -> Double {
        return start + (stop - start) * amt
    }
    
    public final class func map(value: Double, start1: Double, stop1: Double, start2: Double, stop2: Double) -> Double {
        return start2 + (stop2 - start2) * ((value - start1) / (stop1 - start1))
    }
    
    public final class func norm(value: Double, start: Double, stop: Double) -> Double {
        return (value - start) / (stop - start)
    }
    
}
