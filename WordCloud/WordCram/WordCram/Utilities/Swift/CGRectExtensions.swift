//  CGRectExtensions.swift

import UIKit

extension CGRect: Hashable {
    
    // Implementation of FNV hash. More info: http://eternallyconfuzzled.com/tuts/algorithms/jsw_tut_hashing.aspx
    public var hashValue: Int {
        let h = { (hash: Int64, field: Int) -> Int64 in
            return hash * 16777619 ^ Int64(field)
        }
        var hash: Int64 = 2166136261
        hash = h(hash, origin.x.hashValue)
        hash = h(hash, origin.y.hashValue)
        hash = h(hash, width.hashValue)
        hash = h(hash, height.hashValue)
        return Int(hash % Int64(Int.max))
    }
    
}
