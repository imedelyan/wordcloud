//  Random.swift

import Foundation

private func arc4random<T: ExpressibleByIntegerLiteral>(type: T.Type) -> T {
    var r: T = 0
    arc4random_buf(&r, Int(MemoryLayout<T>.stride)) // sizeof(T)
    return r
}

public extension Double {
    public static func random(_ lower: Double = 0.0, _ upper: Double = 1.0) -> Double {
        let r = Double(arc4random(type: UInt64.self)) / Double(UInt64.max)
        return (r * (upper - lower)) + lower
    }
}

public struct GaussianGenerator: IteratorProtocol {
    private var nextNextGaussian: Double?
    
    init() {}
    
    public mutating func next() -> Double? {
        if let nextNextGaussian = nextNextGaussian {
            self.nextNextGaussian = nil
            return nextNextGaussian
        }
        
        var v1 = 0.0, v2 = 0.0, s = 0.0
        repeat {
            v1 = 2 * Double.random() - 1
            v2 = 2 * Double.random() - 1
            s = v1 * v1 + v2 * v2
        } while s >= 1 || s == 0
        
        let multiplier = sqrt(-2 * log(s) / s)
        nextNextGaussian = v2 * multiplier
        return v1 * multiplier
    }
}
