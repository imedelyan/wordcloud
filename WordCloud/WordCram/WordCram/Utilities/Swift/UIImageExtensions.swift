//  UIImageExtensions.swift

import UIKit

public extension UIImage {
    
    public func colorAtPixel(point: CGPoint) -> UIColor? {
        if !CGRect(x: 0, y: 0, width: size.width, height: size.height).contains(point) {
            return nil
        }
        
        let pointX = trunc(point.x)
        let pointY = trunc(point.y)
        let cgImage = self.cgImage!
        /*
        let width = size.width
        let height = size.height
        let bitsPerComponent: UInt = 8
        let bytesPerPixel: UInt = 4
        let bytesPerRow: UInt = bytesPerPixel * 1
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let pixelData = malloc(CUnsignedLong(4))
        */
        let width = cgImage.width
        let height = cgImage.height
        let bitsPerComponent = cgImage.bitsPerComponent
        let bytesPerRow = cgImage.bytesPerRow
        let colorSpace = cgImage.colorSpace
        let bitmapInfo = cgImage.bitmapInfo
//        let pixelData = malloc(CUnsignedLong(4))
        let pixelData = malloc(CLong(4))
        
        let context = CGContext(data: pixelData, width: 1, height: 1, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace!, bitmapInfo: CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue).rawValue)
        
//        CGContextSetBlendMode(context, kCGBlendModeCopy)
        context?.setBlendMode(.copy)
        context!.translateBy(x: -pointX, y: pointY - CGFloat(height))
//        CGContextDrawImage(context, CGRect(x: 0, y: 0, width: CGFloat(width), height: CGFloat(height)), cgImage)
        context?.draw(cgImage, in: CGRect(x: 0, y: 0, width: CGFloat(width), height: CGFloat(height)))
        
        let data = context!.data?.assumingMemoryBound(to: UInt8.self)
        let dataType = UnsafePointer<UInt8>(data)!

        let red = CGFloat(dataType[0]) / 255
        let green = CGFloat(dataType[1]) / 255
        let blue = CGFloat(dataType[2]) / 255
        let alpha = CGFloat(dataType[3]) / 255
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }

}
