//  PlacerHeatMap.swift

import UIKit

extension Bool {
    var intValue: Int {
        return self ? 1 : 0
    }
}

public class PlacerHeatMap {

    private var words: [WCWord]
    private var fonter: WordFonter
    private var sizer: WordSizer
    private var angler: WordAngler
    private var placer: WordPlacer
    private var nudger: WordNudger
    private var shaper: WordShaper
    
    init(words: [WCWord], fonter: WordFonter, sizer: WordSizer, angler: WordAngler, placer: WordPlacer, nudger: WordNudger, shaper: WordShaper) {
        self.words = words
        self.fonter = fonter
        self.sizer = sizer
        self.angler = angler
        self.placer = placer
        self.nudger = nudger
        self.shaper = shaper
    }
    
    class RectStack {
        var rects = [CGRect]()
        
        func add(rect: CGRect) {
            rects.append(rect)
        }
        
        func howManyIntersects(rect: CGRect) -> Int {
            return rects.reduce(0) {
                $0 + rect.intersects($1).intValue
            }
        }
    }
    
    class RectGrid {
        var stack: RectStack
        var levels = [CGRect: Int]() // Note: CGRect is made to conform to Hashable protocol.
        
        init(stack: RectStack) {
            self.stack = stack
        }
        
        func measure(x: Int, y: Int, w: Int, h: Int) {
            let plot = CGRect(x: x, y: y, width: w, height: h)
            let level = stack.howManyIntersects(rect: plot)
            levels[plot] = level
        }
        
        func maxLevel() -> Int {
//            return Array(levels.values).reduce(0, combine: max)
            return Array(levels.values).sorted(by: { $0 > $1 }).first!
        }
        
        func draw(context: CGContext) {
            let rowHeight = 10
            let colWidth = 10
            let rows = Int(context.height / rowHeight)
            let cols = Int(context.width / colWidth)
            
            for i in 0..<rows {
                let y = i * rowHeight
                for j in 0..<cols {
                    let x = j * colWidth
                    measure(x: x, y: y, w: colWidth, h: rowHeight)
                }
            }
            
            _ = maxLevel()
            for (rect, level) in levels {
                var c = UIColor.white.cgColor
                
                if level > 0 {
                    let scaled = Double(level) / 8
                    let hue = Int(PApplet.map(value: scaled, start1: 0, stop1: 1, start2: 85, stop2: 0))
                    c = UIColor(hue: CGFloat(hue), saturation: 255, brightness: 255, alpha: 1).cgColor
                }
                
                context.setFillColor(c.components!)
                context.fill(rect)
            }
        }
    }
    
    public func draw(context: CGContext) {
        let rectStack = RectStack()
        
        for (i, word) in words.enumerated() {
            let wordFont = word.getFont(fonter: fonter)
            let wordSize = word.getSize(sizer: sizer, rank: i, wordCount: words.count)
            let wordAngle = word.getAngle(angler: angler)
            
            let shape = shaper.getShapeFor(word: word.word, font: wordFont, fontSize: CGFloat(wordSize), angle: wordAngle)
            
            let wordImageWidth = Int(shape.width)
            let wordImageHeight = Int(shape.height)
            let sketchWidth = Int(context.width)
            let sketchHeight = Int(context.height)
            
            let desiredLocation = word.getTargetPlace(placer: placer, rank: i, count: words.count, wordImageWidth: wordImageWidth, wordImageHeight: wordImageHeight, fieldWidth: sketchWidth, fieldHeight: sketchHeight)
            
            rectStack.add(rect: CGRect(x: desiredLocation.x, y: desiredLocation.y, width: CGFloat(wordImageWidth), height: CGFloat(wordImageHeight)))
        }
        
        let grid = RectGrid(stack: rectStack)
        grid.draw(context: context)
    }

}
