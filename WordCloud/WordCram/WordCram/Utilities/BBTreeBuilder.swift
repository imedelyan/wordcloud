//  BBTreeBuilder.swift

// Finished

import UIKit

public class BBTreeBuilder {
    
    public func makeTree(shape: CGRect, swelling: Int) -> BBTree? {
        let minBoxSize = 1
        let x = Int(shape.origin.x)
        let y = Int(shape.origin.y)
        let right = x + Int(shape.width)
        let bottom = y + Int(shape.height)
        
        let tree = makeTree(shape: shape, minBoxSize: minBoxSize, x: x, y: y, right: right, bottom: bottom)
        tree?.swell(extra: swelling)
        return tree
    }
    
    private func makeTree(shape: CGRect, minBoxSize: Int, x: Int, y: Int, right: Int, bottom: Int) -> BBTree? {
        let width = right - x
        let height = bottom - y
        let otherShape = CGRect(x: x, y: y, width: width, height: height)
        
        if shape.contains(otherShape) {
            return BBTree(left: x, top: y, right: right, bottom: bottom)
        } else if shape.intersects(otherShape) {
            let tree = BBTree(left: x, top: y, right: right, bottom: bottom)
            
            let tooSmallToContinue = width <= minBoxSize
            if !tooSmallToContinue {
                let centerX = avg(x, right)
                let centerY = avg(y, bottom)
                
                // upper left
                let t0 = makeTree(shape: shape, minBoxSize: minBoxSize, x: x, y: y, right: centerX, bottom: centerY)
                // upper right
                let t1 = makeTree(shape: shape, minBoxSize: minBoxSize, x: centerX, y: y, right: right, bottom: centerY)
                // lower left
                let t2 = makeTree(shape: shape, minBoxSize: minBoxSize, x: x, y: centerY, right: centerX, bottom: bottom)
                // lower right
                let t3 = makeTree(shape: shape, minBoxSize: minBoxSize, x: centerX, y: centerY, right: right, bottom: bottom)
                
                tree.addKids(kids: t0, t1, t2, t3)
            }
            
            return tree
        }
        
        return nil
    }
    
    private func avg(_ a: Int, _ b: Int) -> Int {
        return (a + b) / 2
    }
    
}
