//  WordSorterAndScaler.swift

import Foundation

class WordSorterAndScaler {
    
    func sortAndScale(rawWords: [WCWord]) -> [WCWord] {
        if rawWords.count == 0 { return [] }
        
        let words = rawWords.sorted(by: { $0 > $1 })
       //IOS9 let words = sorted(rawWords) { $0 > $1 }
        let maxWeight = words.first!.weight
        
        
        //???????
        words.map { $0.weight /= maxWeight }
        
        return words
    }
    
}
