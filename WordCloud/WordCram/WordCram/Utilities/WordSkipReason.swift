//  WordSkipReason.swift

import Foundation

public enum WordSkipReason: String {
    case WasOverMaxNumberOfWords = "we already reached the maxNumberOfWordsToDraw threshold"
    case ShapeWasTooSmall = "it was below the minShapeSize threshold"
    case NoSpace = "there wasn't enough space near where you wanted it placed"
}
