//  WordSizer.swift

import Foundation

public protocol WordSizer {
    func sizeFor(word: WCWord, wordRank: Int, wordCount: Int) -> Double
}
