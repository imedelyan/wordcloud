//  WordColorer.swift

import UIKit

public protocol WordColorer {
    func colorFor(word: WCWord) -> UIColor
}
