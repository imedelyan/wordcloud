//  WordAngler.swift

import Foundation

public protocol WordAngler {
    func angleFor(word: WCWord) -> Double
}
