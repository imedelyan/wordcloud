//  Observer.swift

import Foundation

public protocol Observer {
    func wordsCounted(words: [WCWord])
    func beginDraw()
    func wordDrawn(word: WCWord)
    func wordSkipped(word: WCWord)
    func endDraw()
}
