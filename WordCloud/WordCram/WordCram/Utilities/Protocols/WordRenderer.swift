//  WordRenderer.swift

import UIKit

protocol WordRenderer {
    func getWidth() -> Int
    func getHeight() -> Int
    func drawWord(word: EngineWord, color: UIColor)
    func finish()
}
