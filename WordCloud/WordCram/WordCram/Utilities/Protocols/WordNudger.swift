//  WordNudger.swift

import UIKit

public protocol WordNudger {
    func nudgeFor(word: WCWord, attemptNumber: Int) -> CGPoint
}
