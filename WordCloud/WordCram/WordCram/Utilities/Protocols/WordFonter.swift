//  WordFonter.swift

import UIKit

public protocol WordFonter {
    func fontFor(word: WCWord) -> UIFont
}
