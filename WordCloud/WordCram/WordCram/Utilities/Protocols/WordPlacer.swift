//  WordPlacer.swift

import UIKit

public protocol WordPlacer {
    func place(word: WCWord, wordIndex: Int, wordsCount: Int, wordImageWidth: Int, wordImageHeight: Int, fieldWidth: Int, fieldHeight: Int) -> CGPoint
}
