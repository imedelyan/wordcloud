//  WordCounter.swift

import Foundation

class WordCounter {
    
    private var cueStopWords: StopWords!
    private var extraStopWords = [String: Bool]() // emulating set functionality with dictionary
    private var excludeNumbers = false
    
    internal init() {}
    
    internal convenience init(cueStopWords: StopWords!) {
        self.init()
        self.cueStopWords = cueStopWords
    }
    
    internal func withExtraStopWords(extraStopWordsString: String) -> WordCounter {
        let stopWordsArray = extraStopWordsString.lowercased().components(separatedBy: " ")
//          IOS9
//          let stopWordsArray = split(extraStopWordsString.lowercaseString) { $0 == " " }
        extraStopWords = stopWordsArray.reduce([:]) {
            (acc: [String: Bool], word: String) in
            var newAcc = acc
            newAcc[word] = true
            return newAcc
        }
        return self
    }
    
    internal func shouldExcludeNumbers(shouldExcludeNumbers: Bool) -> WordCounter {
        excludeNumbers = shouldExcludeNumbers
        return self
    }
    
    internal func count(text: String, renderOptions: RenderOptions) -> [WCWord] {
        if cueStopWords == nil {
            
            tellScripterAboutTheGuess(stopWords: cueStopWords)
        }
        return countWords(text: text)
    }
    
    private func tellScripterAboutTheGuess(stopWords: StopWords?) {
        if stopWords == nil {
            print("cue.language can't guess what language your text is in")
        } else {
            print("cue.language guesses your text is in \(String(describing: stopWords))")
        }
    }
    
    private func countWords(text: String) -> [WCWord] {
        let counter = Counter<String>()

        var wordIterator = WordIterator(text: text)
        while let word = wordIterator.next() {
            if shouldCountWord(word: word) {
                counter.note(item: word)
            }
        }
        
        let words = counter.entrySet().map { WCWord(word: $0.0, weight: Double($0.1)) }
        return words
    }
    
    private func shouldCountWord(word: String) -> Bool {
        return !isStopWord(word: word) && !(excludeNumbers && isNumeric(word: word))
    }
    
    private func isNumeric(word: String) -> Bool {
        return NumberFormatter().number(from: word) != nil
    }
    
    private func isStopWord(word: String) -> Bool {
        let cueSaysStopWord = cueStopWords != nil && cueStopWords.isStopWord(s: word)
        let extraSaysStopWord = extraStopWords[word] != nil
        return cueSaysStopWord || extraSaysStopWord
    }
    
}
