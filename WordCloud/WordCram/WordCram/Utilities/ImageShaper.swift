//  ImageShaper.swift

import UIKit

public class ImageShaper {
    
    public func shape(image: UIImage, color: UIColor) -> CGRect {
        let tree = RectTree(left: 0, top: 0, right: Int(image.size.width), bottom: Int(image.size.height))
        return tree.toShape(image: image, color: color)
    }
    
    class RectTree {
        var kids: [RectTree]!
        
        var left: Int
        var top: Int
        var right: Int
        var bottom: Int
        var width: Int
        var height: Int
        
        init(left: Int, top: Int, right: Int, bottom: Int) {
            self.left = left
            self.top = top
            self.right = right
            self.bottom = bottom
            
            width = right - left
            height = bottom - top
            
            split()
        }
        
        private func split() {
            /*
            Saying width < 2 OR hegiht < 2 means we miss a few odd pixels.
            Saying width < 2 AND height < 2 means we get them, but it goes a bit slower.
            For now, we'll go with faster.
            */
            if width < 2 || height < 2 { return }
            
            let centerX = avg(left, right)
            let centerY = avg(top, bottom)
            kids = [RectTree]()
            kids.append(RectTree(left: left, top: top, right: centerX, bottom: centerY))
            kids.append(RectTree(left: centerX, top: top, right: right, bottom: centerY))
            kids.append(RectTree(left: left, top: centerY, right: centerX, bottom: bottom))
            kids.append(RectTree(left: centerX, top: centerY, right: right, bottom: bottom))
        }
        
        private func avg(_ a: Int, _ b: Int) -> Int {
            return (a + b) / 2
        }
        
        func toShape(image: UIImage, color: UIColor) -> CGRect {
            if isAllCovered(image: image, color: color) {
                return CGRect(x: left, y: top, width: width, height: height)
            } else {
                return kids.reduce(CGRect.zero) {
                    (acc: CGRect, kid: RectTree) in
                    return acc.union(kid.toShape(image: image, color: color))
                }
            }
        }
        
        private var isAllCoveredMemo: Bool?
        private func isAllCovered(image: UIImage, color: UIColor) -> Bool {
            if isAllCoveredMemo == nil {
                if kids == nil {
                    isAllCoveredMemo = image.colorAtPixel(point: CGPoint(x: left, y: top)) == color
                } else {
                    isAllCoveredMemo = kids.reduce(true) {
                        (acc: Bool, kid: RectTree) in
                        return acc && kid.isAllCovered(image: image, color: color)
                    }
                }
            }
            return isAllCoveredMemo!
        }
    }
    
}
