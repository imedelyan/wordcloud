//  WordShaper.swift

import UIKit

public class WordShaper {
    
    private var context: CGContext
    private var rightToLeft: Bool
    
    public init(context: CGContext, rightToLeft: Bool) {
        self.context = context
        self.rightToLeft = rightToLeft
    }

    public convenience init(context: CGContext) {
        self.init(context: context, rightToLeft: false)
    }
    
    public func getShapeFor(word: String, font: UIFont, fontSize: CGFloat, angle: Double) -> CGRect {
               
//
//        let attrs = [NSFontAttributeName: font.fontWithSize(fontSize)]
//        let string = CFAttributedStringCreate(nil, word, attrs)
//        let line = CTLineCreateWithAttributedString(string)
//        let rect = CGRect(origin: CGPointZero, size: CTLineGetImageBounds(line, context).size)
//        return rect
//        
        
        let attrs = [NSAttributedString.Key.font: font.withSize(fontSize)]
        let string = CFAttributedStringCreate(nil, word as CFString, attrs as CFDictionary)
        let line = CTLineCreateWithAttributedString(string!)
        let lineBounds = CTLineGetImageBounds(line, context)
        let rect = lineBounds.integral
        return rect
    }
    
    public func getShapeFor(word: String, font: UIFont) -> CGRect {
        return getShapeFor(word: word, font: font, fontSize: font.pointSize, angle: 0)
    }
    
    
    
    private func rotate(shape: CGRect, rotation: Double) -> CGRect {
        
        return CGRect.null // TODO: implement
    }

    private func moveToOrigin(shape: CGRect) -> CGRect {
        return shape.offsetBy(dx: -shape.origin.x, dy: -shape.origin.y)
    }
    
}
