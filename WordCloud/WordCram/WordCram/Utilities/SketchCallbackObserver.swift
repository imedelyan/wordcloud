//  SketchCallbackObserver.swift

import UIKit

class SketchCallbackObserver: Observer {

    private var delegate: Observer?
    
    init(delegate: Observer?) {
        self.delegate = delegate
    }
    
    func wordsCounted(words: [WCWord]) {
        delegate?.wordsCounted(words: words)
    }
    
    func beginDraw() {
        delegate?.beginDraw()
    }
    
    func wordDrawn(word: WCWord) {
//        println("drawn word \(word)")
        delegate?.wordDrawn(word: word)
    }
    
    func wordSkipped(word: WCWord) {
//        println("skipped word \(word)")
        delegate?.wordSkipped(word: word)
    }
    
    func endDraw() {
//        println("drawing end")
        delegate?.endDraw()
    }
        
}
