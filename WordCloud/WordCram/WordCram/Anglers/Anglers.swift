//  Anglers.swift

import UIKit

public class Anglers {
    
    class RandomWordAngler: WordAngler {
        func angleFor(word: WCWord) -> Double {
            // return Double(random()) * 2 / Double.pi
            return Double.random() * 2 / Double.pi
        }
    }
    
    class RandomBetweenAngler: WordAngler {
        private var min: Double
        private var max: Double
        
        init(_ min: Double, _ max: Double) {
            self.min = min
            self.max = max
        }
        
        func angleFor(word: WCWord) -> Double {
            let difference = max - min
//            return Double(random()) * difference + min
            return Double.random() * difference + min
        }
    }
    
    class AlwaysUseAngler: WordAngler {
        private var angle: Double
        
        init(_ angle: Double) {
            self.angle = angle
        }
        
        func angleFor(word: WCWord) -> Double {
            return angle
        }
    }
    
    class PickFromAngler: WordAngler {
        private var angles: [Double]
        
        init(_ angles: [Double]) {
            self.angles = angles
        }
        
        func angleFor(word: WCWord) -> Double {
            let ind = Int(arc4random_uniform(UInt32(angles.count)))
            return angles[ind]
        }
    }
    
    
    public class func random() -> WordAngler {
        return RandomWordAngler()
    }
    
    public class func randomBetween(_ min: Double, _ max: Double) -> WordAngler {
        return RandomBetweenAngler(min, max)
    }
    
    public class func heaped() -> WordAngler {
        let angle = 7 * Double.pi / 180
        return randomBetween(angle, -angle)
    }
    
    public class func alwaysUse(angle: Double) -> WordAngler {
        return AlwaysUseAngler(angle)
    }
    
    public class func pickFrom(angles: Double...) -> WordAngler {
        return PickFromAngler(angles)
    }
    
    public class func pickFrom(angles: [Double]) -> WordAngler {
        return pickFrom(angles: angles)
    }
    
    public class func horiz() -> WordAngler {
        return alwaysUse(angle: 0)
    }
    
    public class func upAndDown() -> WordAngler {
        return pickFrom(angles: Double.pi / 2, -Double.pi / 2)
    }
    
    public class func mostlyHoriz() -> WordAngler {
        return pickFrom(angles: 0, 0, 0, 0, 0, Double.pi / 2, -Double.pi / 2)
    }
    
}
