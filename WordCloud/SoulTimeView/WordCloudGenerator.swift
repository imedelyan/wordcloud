//
//  WordCloudGenerator.swift
//  WordCloud
//
//  Created by Igor Medelian on 2/26/19.
//  Copyright © 2019 imedelian. All rights reserved.
//

import UIKit

open class WordCloudGenerator {

    private let fontName: String
    private var minFontSize: Float = 20
    private var maxFontSize: Float = 70
    private var words: [CloudWord] = []
    private var wordCloudView = UIView()
    private var labels: [UILabel] = []
//    private var isWordsAmountChanged = false
    private let size = CGSize(width: 1000, height: 800)
    private typealias GenerateCloudCompletion = () -> Void
    private typealias AddLabelCompletion = (_ success: Bool) -> Void
    
    public init(fontName: String = "") {
        self.fontName = fontName
    }
    
    public func generate(from words: [CloudWord], completion: @escaping (UIImage) -> Void) {
        wordCloudView = UIView(frame: CGRect(origin: .zero, size: size))
        self.words = words
        generate() { [weak self] in
            guard let self = self else { return }
            let renderer = UIGraphicsImageRenderer(size: self.wordCloudView.bounds.size)
            let image = renderer.image { ctx in
                self.wordCloudView.drawHierarchy(in: self.wordCloudView.bounds, afterScreenUpdates: true)
            }
            completion(image)
        }
    }
    
    private func generate(completion: @escaping GenerateCloudCompletion) {
        guard words.count > 0 else { completion(); return }
        
        if labels.count > 0 {
            labels.forEach { $0.removeFromSuperview() }
            labels.removeAll()
        }
        
        let wordsToDrawCountIndex = words.count - 1
//        // draw the last word first on it's position
//        var wordsToDrawCountIndex = 0
//        if !isWordsAmountChanged {
//            wordsToDrawCountIndex = words.count - 2
//            addLastWordLabel()
//        } else {
//            wordsToDrawCountIndex = words.count - 1
//        }
        
        guard wordsToDrawCountIndex >= 0 else { completion(); return }
        for i in 0...wordsToDrawCountIndex {
            let label = wordLabel(for: i)
            add(label: label)
        }
        
        completion()
    }
    
//    private func addLastWordLabel() {
//        let label = wordLabel(for: words.endIndex - 1)
//
//        var labelCenterX = wordCloudView.bounds.width / 2
//        let labelCenterY = wordCloudView.bounds.height / 2
//        let step: CGFloat = 1
//        var cntTry = 0
//
//        repeat {
//            let x = labelCenterX - label.bounds.width / 2
//            let y = labelCenterY - label.bounds.height / 2
//            label.frame = CGRect(x: x, y: y, width: CGFloat(label.bounds.width), height: CGFloat(label.bounds.height))
//            guard cntTry < 500 else { return }
//            cntTry += 1
//            labelCenterX -= step
//        } while frameIntersects(label.frame)
//
//        labelCenterX += step
//        labels.append(label)
//        wordCloudView.addSubview(label)
//    }
    
    private func add(label: UILabel) {
        let centerX = wordCloudView.bounds.width / 2
        let centerY = wordCloudView.bounds.height / 2
        var labelCenterX = centerX
        var labelCenterY = centerY
        var radius: CGFloat = 0
        var angle: CGFloat = 0
        let step: CGFloat = 0.5
        var cntTry = 0
        
        repeat {
            labelCenterX = centerX + radius * cos(angle)
            labelCenterY = centerY + radius * sin(angle)
            let x = labelCenterX - label.bounds.width / 2
            let y = labelCenterY - label.bounds.height / 2
            label.frame = CGRect(x: x, y: y, width: CGFloat(label.bounds.width), height: CGFloat(label.bounds.height))
            guard cntTry < 12000 else { return }
            cntTry += 1
            radius += 0.05
            angle += step / radius
        } while frameIntersects(label.frame)
        
        labels.append(label)
        wordCloudView.addSubview(label)
    }
    
    private func wordLabel(for index: Int) -> UILabel {
        let label = UILabel()
        label.text = words[index].title
        label.textColor = words[index].color
        label.font = sizeRatioFont(words[index].weight)
        label.sizeToFit()
        label.frame = CGRect(x: 0, y: 0, width: label.frame.width + 10, height: label.frame.height)
        return label
    }
    
    private func sizeRatioFont(_ ratio: Float) -> UIFont {
        let fontSize = CGFloat(ratio / 10 * (maxFontSize - minFontSize) + minFontSize)
        return UIFont(name: fontName, size: fontSize) ?? UIFont.boldSystemFont(ofSize: fontSize)
    }
    
    private func frameIntersects(_ frame: CGRect) -> Bool {
        guard frame.maxX < wordCloudView.frame.width, frame.maxY < wordCloudView.frame.height, frame.minX > 0, frame.minY > 0 else { return true }
        for label in labels where frame.intersects(label.frame) { return true }
        return false
    }
}
