//
//  SoulCloudViewController.swift
//  WordCloud
//
//  Created by Igor Medelian on 2/28/19.
//  Copyright © 2019 imedelian. All rights reserved.
//

import UIKit

class SoulCloudViewController: UIViewController {

    @IBOutlet weak var wordCloudImageView: UIImageView!
    @IBOutlet weak var fontSizeSlider: UISlider!
    
    let wordCloudGenerator = WordCloudGenerator()
    
    let red = UIColor(red: 232/255, green: 101/255, blue: 148/255, alpha: 1.0)
    let blue = UIColor(red: 69/255, green: 166/255, blue: 242/255, alpha: 1.0)
    let green = UIColor(red: 103/255, green: 187/255, blue: 106/255, alpha: 1.0)
    let yellow = UIColor(red: 231/255, green: 175/255, blue: 43/255, alpha: 1.0)
    
    let cloudWordsCount = 18
    
    lazy var words: [CloudWord] = [CloudWord(title: "Downhearted", weight: 1, color: red),
                                   CloudWord(title: "Worn-out", weight: 1, color: blue),
                                   CloudWord(title: "Hopeful", weight: 1, color: green),
                                   CloudWord(title: "Brave", weight: 1, color: yellow),
                                   
                                   CloudWord(title: "Anxious", weight: 1, color: red),
                                   CloudWord(title: "Stressed", weight: 1, color: blue),
                                   CloudWord(title: "Calm", weight: 1, color: green),
                                   CloudWord(title: "Peaceful", weight: 1, color: yellow),
                                   
                                   CloudWord(title: "A Failure", weight: 1, color: red),
                                   CloudWord(title: "Inadequate", weight: 1, color: blue),
                                   CloudWord(title: "Content", weight: 1, color: green),
                                   CloudWord(title: "Valuable", weight: 1, color: yellow),
                                   
                                   CloudWord(title: "Ashamed", weight: 1, color: red),
                                   CloudWord(title: "Self-critical", weight: 1, color: blue),
                                   CloudWord(title: "Accepting", weight: 1, color: green),
                                   CloudWord(title: "Happy", weight: 1, color: yellow),
                                   
                                   CloudWord(title: "Disconnected", weight: 1, color: red),
                                   CloudWord(title: "Distant", weight: 1, color: blue),
                                   CloudWord(title: "Patient", weight: 1, color: green),
                                   CloudWord(title: "Kind", weight: 1, color: yellow),
                                   
                                   CloudWord(title: "Beaten", weight: 1, color: red),
                                   CloudWord(title: "Frustrated", weight: 1, color: blue),
                                   CloudWord(title: "Positive", weight: 1, color: green),
                                   CloudWord(title: "Ustoppable", weight: 1, color: yellow)]
    
    var chosenWords: [CloudWord] = [] {
        didSet {
            guard chosenWords.count > 0 else {
                wordCloudImageView.image = nil
                return
            }
            
            let eachWordCount = Int((Double(cloudWordsCount) / Double(chosenWords.count)).rounded(.up))
            cloudWords = []
            for _ in 0...eachWordCount - 1 {
                for i in 0...chosenWords.count - 1 {
                    cloudWords.append(chosenWords[i])
                }
            }
            cloudWords = Array(cloudWords[0..<18])
            wordCloudGenerator.generate(from: cloudWords) { [weak self] image in
                self?.wordCloudImageView.image = image
            }
        }
    }
    
    var cloudWords: [CloudWord] = []
    
    @IBAction func didTapWord(_ sender: WordButton) {
        let tappedWord = words[sender.tag]
        if chosenWords.filter({ $0.title == tappedWord.title }).count > 0 {
            chosenWords = chosenWords.filter { $0.title != tappedWord.title }
            chosenWords.count > 0
                ? fontSizeSlider.setValue(chosenWords[0].weight, animated: true)
                : fontSizeSlider.setValue(0, animated: true)
            sender.isSelected = false
        } else {
            chosenWords.insert(words[sender.tag], at: cloudWords.startIndex)
            fontSizeSlider.setValue(0, animated: true)
            sender.isSelected = true
        }
    }
    
    @IBAction func didMoveFontSizeSlider(_ sender: UISlider) {
        chosenWords[0].weight = sender.value
    }
}

class WordButton: UIButton {
    
    @IBInspectable var radius: CGFloat = 3.0
    
    
    @IBInspectable public var borderColor: UIColor = .clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable public var borderWidth: CGFloat = 1.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable public var normalBackgroundColor: UIColor = .clear
    @IBInspectable public var selectedBackgroundColor: UIColor = .clear
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    
    override var isSelected: Bool {
        didSet {
            backgroundColor = isSelected ? selectedBackgroundColor : normalBackgroundColor
        }
    }
    
    func setup() {
        layer.cornerRadius = radius
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
    }
}
